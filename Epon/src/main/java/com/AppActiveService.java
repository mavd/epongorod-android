package com;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.epon.EponApp;

/**
 * Created by macbook on 03/01/15.
 */
public class  AppActiveService extends Service {
    // Receives interactions from clients:
    private final IBinder mBinder = new AppActiveBinder();

    /**
     * Provides a handle to the bound service.
     */
    public class  AppActiveBinder extends Binder {
        public AppActiveService getService() {
            return AppActiveService.this;
        }
    }

    @Override
    public void onCreate(){
    }

    @Override
    public void onDestroy(){
        // TODO: Here is presumably "application level" pause
        int u=1;
        u++;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}