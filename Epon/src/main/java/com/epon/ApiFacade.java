package com.epon;

import com.android.volley.RequestQueue;
import com.epon.network.CheckRequest;
import com.epon.network.ImageRequest;
import com.epon.network.Listener;
import com.epon.network.OrderRequest;
import com.epon.network.SqlRequest;

/**
 * Created by macbook on 28/12/14.
 */
public class ApiFacade {
    static  ApiFacade _singletone;
    public static ApiFacade getInstance(){
        if (_singletone==null){
            _singletone= new ApiFacade();
        }
        return _singletone;
    }

    protected RequestQueue getQueue() {
        return EponApp.getInstance().getRequestQueue();
    }

    public void checkUpdates(Listener listener){
        CheckRequest checkRequest= new CheckRequest(listener);
        getQueue().add(checkRequest);
    }

    public void getDBFromServer(String version,Listener listener){
        SqlRequest sqlRequest=new SqlRequest(version,listener);
        getQueue().add(sqlRequest);
    }

    public void getImages(Listener listener){

        ImageRequest imageRequest= new ImageRequest(listener);
        getQueue().add(imageRequest);
    }

    public void sentOrder(String order,Listener listener){
        OrderRequest orderRequest=new OrderRequest(order, listener);
        getQueue().add(orderRequest);
    }

}

