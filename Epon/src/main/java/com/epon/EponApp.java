package com.epon;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.DataManager;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.epon.DataBase.DBHelper;
import com.epon.DataBase.helpers.AllHelper;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

/**
 * Created by macbook on 28/12/14.
 */
public class EponApp extends Application {
    static  EponApp _singletone;
    public  static final String SERVER_URL="http://epongorod.mobissa.ru/api/?action=";
    private RequestQueue mRequestQueue;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Toast toast;
    private AllHelper allHelper;
    private DataManager dataManager;

    public static final int PORTRAIT=0;
    public static final int LANDSCAPE=1;
    DBHelper dbHelper;
    SQLiteDatabase db;


    public int id_usuall;
    public int id_spec;
    public  int id_post;
    public  int id_epon;
    public int id_uno;
    ImageLoader imageLoader;


    public static EponApp getInstance(){
        if (_singletone == null) {
            _singletone = new EponApp();
            _singletone.onCreate();
        }
        return _singletone;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        _singletone= this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        dbHelper= new DBHelper(this);
        db= dbHelper.getWritableDatabase();
        clearPrice();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .memoryCacheExtraOptions(300, 300).diskCacheExtraOptions(480, 800, null)// width, height
                .threadPoolSize(5)
                .threadPriority(Thread.MIN_PRIORITY + 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new UsingFreqLimitedMemoryCache(5 * 1024 * 1024)) // 2 Mb
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(5 * 1024 * 1024))
                .memoryCacheSize(5 * 1024 * 1024) // connectTimeout (5 s), readTimeout (30 s)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();
        ImageLoader.getInstance().init(config);

        //allHelper= new AllHelper(getApplicationContext());
    }

    public void clearPrice(){
        db.execSQL(DBHelper.SQL_DELETE_PRICE);
        db.execSQL(DBHelper.SQL_CREATE_PRICE);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(this);
        }
        return mRequestQueue;
    }

    public void setVersion(String version){
        editor.putString("version", version);
        editor.commit();
    }
    public  String getVersion(){
        return  sharedPreferences.getString("version",null);
    }

    public void setNextBanner(int banner){
        editor.putInt("banner", banner);
        editor.commit();
    }
    public int getNextBanner(){
        return  sharedPreferences.getInt("banner",0);
    }

    public void showToast(String message) {
        if (toast != null) {
            toast.cancel();
        }
        int duration = Toast.LENGTH_LONG;
        toast = Toast.makeText(this, message, duration);
        toast.show();
    }

    public AllHelper getAllHelper() {
        if (allHelper==null){
            allHelper = new AllHelper(getApplicationContext());
        }
        return allHelper;
    }
    public int getAmountOfOrder(String name){
        Cursor mCursor = db.rawQuery("select * from orderTable where name=?", new String[] {name});
        boolean exists = (mCursor.getCount() > 0);
        int amount;
        if (exists) {
            mCursor.moveToFirst();
            amount=mCursor.getInt(mCursor.getColumnIndex("amount"));
            return amount;
        }
        else return 1;
    }


}
