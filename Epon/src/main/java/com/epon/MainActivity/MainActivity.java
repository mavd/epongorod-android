package com.epon.MainActivity;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.AppActiveService;
import com.epon.ChoosingActivity.AboutActivity;
import com.epon.DataBase.DBHelper;
import com.epon.DataBase.Tag;
import com.epon.DataBase.helpers.TagHelper;
import com.epon.EponApp;
import com.epon.R;

import com.epon.fragments.BasketFragment;
import com.epon.fragments.ListFragmentPizza;
import com.epon.fragments.ListFragmentPizzaLanten;
import com.epon.fragments.ListFragmentSushi;
import com.epon.fragments.ListFragmentSushiLanten;
import com.epon.fragments.ListFragmentSushiSpec;
import com.epon.fragments.SubCategory;
import com.epon.fragments.SummaryItemFragment;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends ActionBarActivity {

    //ActionBar
    private ActionBar mActionBar;
    Menu m;
    ViewGroup actionBarLayout;

    //ViewPager
    ViewPager mPager;
    MyAdapter mAdapter;
    MySecondAdapter mySecondAdapter;

    //Header of ViewPager
    private static final String[] titles = { "ОБЫЧНОЕ МЕНЮ", "ПОСТНОЕ МЕНЮ", "СПЕЦПРЕДЛОЖЕНИЕ"};

    //Fragments
    android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
    BasketFragment basketFragment;
    SubCategory suCategory;
    SummaryItemFragment summaryItemFragment;
    FragmentTransaction fragmentTransaction;
    //Menu elements
    MenuItem sushi;
    MenuItem pizza;
    MenuItem basket;
    MenuItem aboutItem;
    MenuItem basket2;
    TextView iv;

    //Layout
    RelativeLayout relativeLayout;

    //DB
    DBHelper dbHelper;

    //ProgressBar
    ProgressBar progressBar;
    GettingBaseMyTask gettingBaseMyTask;

    //Intent stuff
    int numberOfChange;
    private ImageView iconSushi,iconPizza;
    //Double Exit
    private boolean doubleBackToExitPressedOnce = false;
    private TagHelper tagHelper;
    private boolean mAppActiveServiceBound = false;
    private AppActiveService mAppActiveService = null;
    private ServiceConnection mAppActiveConnection = new ServiceConnection() {
        public void onServiceConnected( ComponentName className, IBinder service ) {
            mAppActiveService = ( (AppActiveService.AppActiveBinder) service ).getService();
        }
        public void onServiceDisconnected( ComponentName className ) {
            mAppActiveService = null;
        }
    };
    private ArrayList<Tag> eponTags= new ArrayList<Tag>();
    private ArrayList<Tag> unoTags= new ArrayList<Tag>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_main);
        tagHelper= EponApp.getInstance().getAllHelper().getTagHelper();
        eponTags=tagHelper.getTagByParentId(EponApp.getInstance().id_epon);
        unoTags=tagHelper.getTagByParentId(EponApp.getInstance().id_uno);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayoutMainActivity);
        actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.actionbar_header_basket, null);
        actionBarLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        progressBar = (ProgressBar) findViewById(R.id.progressBarMainActivity);

        //DB
        dbHelper = new DBHelper(this);

        //ViewPager
        mAdapter = new MyAdapter(getSupportFragmentManager(),eponTags);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

         //ActionBar
        mActionBar = getSupportActionBar();
        mActionBar.setIcon(R.drawable.logo_action_bar);
        mActionBar.setDisplayShowTitleEnabled(false);

        Intent intent = getIntent();
        numberOfChange = intent.getIntExtra("numberOfChange", 0);
        /*gettingBaseMyTask = new GettingBaseMyTask();
        gettingBaseMyTask.execute();*/


    }

    //ActionBarMenu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        m = menu;
        getMenuInflater().inflate(R.menu.main, menu);

        //Get Custom Item menu
        basket2 = m.findItem(R.id.bsketWithPrice);
        basket2.setActionView(R.layout.actionbar_item_basket);
        sushi = m.findItem(R.id.sushi);
        //sushi.setActionView(R.layout.menu_item_sushi);
        pizza = m.findItem(R.id.pizza);
        //pizza.setActionView(R.layout.menu_item_sushi);
        TextView txtPizza=(TextView)pizza.getActionView().findViewById(R.id.txtMenu);
        txtPizza.setText(getString(R.string.pizza));
        iconSushi=(ImageView)sushi.getActionView().findViewById(R.id.imgIcon);
        iconSushi.setImageDrawable(getResources().getDrawable(R.drawable.sushi_icon_focused));
        iconPizza=(ImageView)pizza.getActionView().findViewById(R.id.imgIcon);
        iconPizza.setImageDrawable(getResources().getDrawable(R.drawable.pizza_icon));
        LinearLayout layoutPizza=(LinearLayout)pizza.getActionView().findViewById(R.id.layoutMain);
        LinearLayout layoutSushi=(LinearLayout)sushi.getActionView().findViewById(R.id.layoutMain);
        LinearLayout layoutBasket=(LinearLayout)basket2.getActionView().findViewById(R.id.layoutMain);
        final View pizzaBottom=pizza.getActionView().findViewById(R.id.bottomLine);
        final View sushiBottom= sushi.getActionView().findViewById(R.id.bottomLine);
        sushiBottom.setVisibility(View.VISIBLE);
        layoutPizza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                    fragmentManager.popBackStack();
                }
                iconPizza.setImageDrawable(getResources().getDrawable(R.drawable.pizza_icon_focused));
                iconSushi.setImageDrawable(getResources().getDrawable(R.drawable.sushi_icon));
                sushiBottom.setVisibility(View.INVISIBLE);
                pizzaBottom.setVisibility(View.VISIBLE);
                pizza.setIcon(R.drawable.pizza_icon_focused);
                relativeLayout.setBackgroundResource(R.drawable.background_uno_menu);
                mActionBar.setIcon(R.drawable.logo_action_bar_uno);
                mPager.setVisibility(View.VISIBLE);
                mySecondAdapter = new MySecondAdapter(getSupportFragmentManager(),unoTags);
                mPager.setAdapter(null);
                mPager.setAdapter(mySecondAdapter);

                basketFragment = (BasketFragment) getSupportFragmentManager().findFragmentByTag("BasketFragment");
                suCategory = (SubCategory) getSupportFragmentManager().findFragmentByTag("SubCategoryFragment");
                summaryItemFragment = (SummaryItemFragment) getSupportFragmentManager().findFragmentByTag("SummaryItemFragment");

                if (basketFragment != null && basketFragment.isVisible()) {
                    fragmentTransaction.remove(basketFragment);
                    fragmentTransaction.commit();

                } else if (suCategory != null && suCategory.isVisible()) {
                    fragmentTransaction.remove(suCategory);
                    fragmentTransaction.commit();

                } else if (summaryItemFragment != null && summaryItemFragment.isVisible()) {
                    fragmentTransaction.remove(summaryItemFragment);
                    fragmentTransaction.commit();
                }


            }
        });
        layoutSushi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                    fragmentManager.popBackStack();
                }
                mAdapter = new MyAdapter(getSupportFragmentManager(),eponTags);
                mPager.setAdapter(mAdapter);
                iconPizza.setImageDrawable(getResources().getDrawable(R.drawable.pizza_icon));
                iconSushi.setImageDrawable(getResources().getDrawable(R.drawable.sushi_icon_focused));
                sushiBottom.setVisibility(View.VISIBLE);
                pizzaBottom.setVisibility(View.INVISIBLE);
                mActionBar.setIcon(R.drawable.logo_action_bar);
                sushi.setIcon(R.drawable.sushi_icon_focused);
                String rotation = getRotation(getApplicationContext());
                if (rotation.equalsIgnoreCase("landscape")) {
                    relativeLayout.setBackgroundResource(R.drawable.background_epon_menu_land);
                } else {
                    relativeLayout.setBackgroundResource(R.drawable.background_epon_menu);
                }
                mPager.setVisibility(View.VISIBLE);
                basketFragment = (BasketFragment) getSupportFragmentManager().findFragmentByTag("BasketFragment");
                suCategory = (SubCategory) getSupportFragmentManager().findFragmentByTag("SubCategoryFragment");
                summaryItemFragment = (SummaryItemFragment) getSupportFragmentManager().findFragmentByTag("SummaryItemFragment");
                if (basketFragment != null && basketFragment.isVisible()) {
                    fragmentTransaction.remove(basketFragment);
                    fragmentTransaction.commit();

                } else if (suCategory != null && suCategory.isVisible()) {
                    fragmentTransaction.remove(suCategory);
                    fragmentTransaction.commit();

                } else if (summaryItemFragment != null && summaryItemFragment.isVisible()) {
                    fragmentTransaction.remove(summaryItemFragment);
                    fragmentTransaction.commit();
                }
            }
        });
        iv = (TextView) basket2.getActionView().findViewById(R.id.priceOrderText);
        iv.setText("0 р.");
        pizza = m.findItem(R.id.pizza);
        layoutBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = fragmentManager.beginTransaction();
                sushi = m.findItem(R.id.sushi);
                sushi.setIcon(R.drawable.sushi_icon);
                pizza = m.findItem(R.id.pizza);
                pizza.setIcon(R.drawable.pizza_icon);
                //aboutItem = m.findItem(R.id.abut);
                setBasket();
            }
        });
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentTransaction = fragmentManager.beginTransaction();

                sushi = m.findItem(R.id.sushi);
                sushi.setIcon(R.drawable.sushi_icon);
                pizza = m.findItem(R.id.pizza);
                pizza.setIcon(R.drawable.pizza_icon);
                //aboutItem = m.findItem(R.id.abut);
                setBasket();
            }
        });
        TextView txtBasket=(TextView)basket2.getActionView().findViewById(R.id.txtBasket);
        if  (getResources().getDisplayMetrics().densityDpi== DisplayMetrics.DENSITY_MEDIUM||
                getResources().getDisplayMetrics().densityDpi== DisplayMetrics.DENSITY_LOW ){
            txtBasket.setVisibility(View.GONE);
        }

        basket = m.findItem(R.id.basket);
        basket.setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    //Select of menu items
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        fragmentTransaction = fragmentManager.beginTransaction();
        sushi = m.findItem(R.id.sushi);
        sushi.setIcon(R.drawable.sushi_icon);
        pizza = m.findItem(R.id.pizza);
        pizza.setIcon(R.drawable.pizza_icon);
        //basket = m.findItem(R.id.basket);
        //basket.setIcon(R.drawable.basket_icon);
        //aboutItem = m.findItem(R.id.abut);
        basket2 = m.findItem(R.id.bsketWithPrice);

        switch (item.getItemId()) {
            case R.id.sushi:
                for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                    fragmentManager.popBackStack();
                }
                mAdapter = new MyAdapter(getSupportFragmentManager(),eponTags);
                mPager.setAdapter(mAdapter);

                mActionBar.setIcon(R.drawable.logo_action_bar);
                sushi.setIcon(R.drawable.sushi_icon_focused);
                String rotation = getRotation(this);

                if (rotation.equalsIgnoreCase("landscape")) {
                    relativeLayout.setBackgroundResource(R.drawable.background_epon_menu_land);
                } else {
                    relativeLayout.setBackgroundResource(R.drawable.background_epon_menu);
                }

                mPager.setVisibility(View.VISIBLE);

                basketFragment = (BasketFragment) getSupportFragmentManager().findFragmentByTag("BasketFragment");
                suCategory = (SubCategory) getSupportFragmentManager().findFragmentByTag("SubCategoryFragment");
                summaryItemFragment = (SummaryItemFragment) getSupportFragmentManager().findFragmentByTag("SummaryItemFragment");

                if (basketFragment != null && basketFragment.isVisible()) {
                    fragmentTransaction.remove(basketFragment);
                    fragmentTransaction.commit();

                } else if (suCategory != null && suCategory.isVisible()) {
                    fragmentTransaction.remove(suCategory);
                    fragmentTransaction.commit();

                } else if (summaryItemFragment != null && summaryItemFragment.isVisible()) {
                    fragmentTransaction.remove(summaryItemFragment);
                    fragmentTransaction.commit();
                }

                break;

            case R.id.pizza:

                for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                    fragmentManager.popBackStack();
                }
                pizza.setIcon(R.drawable.pizza_icon_focused);
                relativeLayout.setBackgroundResource(R.drawable.background_uno_menu);
                mActionBar.setIcon(R.drawable.logo_action_bar_uno);
                mPager.setVisibility(View.VISIBLE);
                mySecondAdapter = new MySecondAdapter(getSupportFragmentManager(),unoTags);
                mPager.setAdapter(null);
                mPager.setAdapter(mySecondAdapter);

                basketFragment = (BasketFragment) getSupportFragmentManager().findFragmentByTag("BasketFragment");
                suCategory = (SubCategory) getSupportFragmentManager().findFragmentByTag("SubCategoryFragment");
                summaryItemFragment = (SummaryItemFragment) getSupportFragmentManager().findFragmentByTag("SummaryItemFragment");

                if (basketFragment != null && basketFragment.isVisible()) {
                    fragmentTransaction.remove(basketFragment);
                    fragmentTransaction.commit();

                } else if (suCategory != null && suCategory.isVisible()) {
                    fragmentTransaction.remove(suCategory);
                    fragmentTransaction.commit();

                } else if (summaryItemFragment != null && summaryItemFragment.isVisible()) {
                    fragmentTransaction.remove(summaryItemFragment);
                    fragmentTransaction.commit();
                }

                break;

            case R.id.basket:
                setBasket();
                break;

            /*case R.id.abut:

                Intent intent = new Intent(this, AboutActivity.class);
                startActivityForResult(intent, 1);*/

            default:
                break;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    //BackButton
    @Override
    public void onBackPressed() {

        suCategory = (SubCategory) getSupportFragmentManager().findFragmentByTag("SubCategoryFragment");
        basketFragment = (BasketFragment) getSupportFragmentManager().findFragmentByTag("BasketFragment");
        summaryItemFragment = (SummaryItemFragment) getSupportFragmentManager().findFragmentByTag("SummaryItemFragment");
        SummaryItemFragment summaryItemFragmentBasket = (SummaryItemFragment) getSupportFragmentManager().findFragmentByTag("SummaryItemFragmentBasket");

        if (suCategory != null && suCategory.isVisible()){
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(suCategory);
            fragmentTransaction.commit();
            for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }
            mPager.setVisibility(View.VISIBLE);

            //super.onBackPressed();

        } else if (basketFragment != null && basketFragment.isVisible()) {

            fragmentTransaction = fragmentManager.beginTransaction();
            //fragmentManager.popBackStack("SummaryItemFragmentBasket",FragmentManager.POP_BACK_STACK_INCLUSIVE);

            fragmentTransaction.remove(basketFragment);
            fragmentTransaction.commit();
            mPager.setVisibility(View.VISIBLE);
            //aboutItem.setVisible(true);
            pizza.setVisible(true);
            basket2.setVisible(true);
            sushi.setVisible(true);

            //mActionBar.setDisplayShowTitleEnabled(true);
            mActionBar.setDisplayShowCustomEnabled(false);
            mActionBar.setDisplayShowHomeEnabled(true);
            mActionBar.setDisplayUseLogoEnabled(true);

            sushi.setIcon(R.drawable.sushi_icon_focused);

            //mActionBar.setIcon(R.drawable.logo_action_bar);
            //mActionBar.setIcon(R.drawable.logo_action_bar);

            String refreshedSumm = getSumm();
            TextView bas = (TextView) this.findViewById(R.id.priceOrderText);
            bas.setText(refreshedSumm);

        } else if (summaryItemFragment != null && summaryItemFragment.isVisible()) {

            /*fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(summaryItemFragment);
            fragmentTransaction.commit();*/
            //mPager.setVisibility(View.VISIBLE);
            pizza.setVisible(true);
            basket2.setVisible(true);
            sushi.setVisible(true);
            mActionBar.setDisplayShowCustomEnabled(false);
            mActionBar.setDisplayShowHomeEnabled(true);
            mActionBar.setDisplayUseLogoEnabled(true);
            TextView bas = (TextView)findViewById(R.id.priceOrderText);
            if (bas!=null){
                bas.setText(getSumm());
            }
            super.onBackPressed();

        } else if (summaryItemFragmentBasket != null && summaryItemFragmentBasket.isVisible()) {

            super.onBackPressed();

        } else if (!doubleBackToExitPressedOnce) {

            for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }

            //Duble Exit Stuff
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Повторное нажатие 'Назад' для выхода", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);

        } else if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

        } else {
            super.onBackPressed();
            android.os.Process.killProcess(android.os.Process.myPid());
        }

    }

    //ViewPager Adapter
    public static class MyAdapter extends FragmentStatePagerAdapter {
        ArrayList<Tag> tags;
        public MyAdapter(FragmentManager fm,ArrayList<Tag> tags) {
            super(fm);
            this.tags=tags;
        }

        @Override
        public int getCount() {
            return tags.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return tags.get(position).title.toUpperCase();
        }

        @Override
        public Fragment getItem(int position) {
           // switch (position) {
             //   case 0:
                    return ListFragmentSushi.newInstance(tags.get(position).id,EponApp.getInstance().id_epon);

               /* case 1:
                    return new ListFragmentSushiLanten();

                case 2:
                    return new ListFragmentSushiSpec();

                default:
                    return null;*/
            //}
        }
    }

    //The second ViewPager Adapter
    public static class MySecondAdapter extends FragmentStatePagerAdapter {
        private ArrayList<Tag> tags;
        public MySecondAdapter(FragmentManager fm,ArrayList<Tag> tags) {
            super(fm);
            this.tags=tags;
        }

        @Override
        public int getCount() {
            return tags.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return tags.get(position).title.toUpperCase();
        }

        @Override
        public Fragment getItem(int position) {
           /* switch (position) {
                case 0:
                    return new ListFragmentPizza();

                case 1:
                    return new ListFragmentPizzaLanten();

                default:
                    return null;
            }*/
            return  ListFragmentSushi.newInstance(tags.get(position).id,EponApp.getInstance().id_uno);
        }
    }

    class GettingBaseMyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            Log.d("NumberOfChange", String.valueOf(numberOfChange));
            if (numberOfChange == 0) {

                //Getting and Parsing of xls-file.
                File file = new File(Environment.getExternalStorageDirectory()
                        .getAbsolutePath(), "BaseEpon.xls");

                InputStream input = null;
                try {
                    input = new FileInputStream(file);

                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                }


                    HSSFWorkbook wb = null;
                    try {
                        POIFSFileSystem fs = new POIFSFileSystem( input );
                        wb = new HSSFWorkbook (fs);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //Getting all rows from xls
                    if (wb != null) {

                        SQLiteDatabase db = dbHelper.getWritableDatabase();
                        dbHelper.onUpgrade(db, 1, 1);
                        ContentValues cv = new ContentValues();
                        Sheet sheet = wb.getSheetAt(0);
                        for (int i = 1; i < sheet.getLastRowNum(); i++) {
                            Row row = sheet.getRow(i);
                            for (int cn=row.getFirstCellNum(); cn<row.getLastCellNum(); cn++) {
                                Cell cell = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
                                String result = cell.toString();
                                String name = "";
                                switch (cn) {
                                    case 0:
                                        name = "type";
                                        break;
                                    case 1:
                                        name = "category";
                                        break;
                                    case 2:
                                        name = "subcategory";
                                        break;
                                    case 3:
                                        name = "name";
                                        break;
                                    case 4:
                                        name = "composition";
                                        break;

                                    case 5:
                                        name = "weight";
                                        break;

                                    case 6:
                                        name = "diameter";
                                        break;
                                    case 7:
                                        name = "price";
                                        break;

                                    case 8:
                                        name = "url";
                                        break;
                                }

                                if(name != "")
                                    cv.put(name, result);
                            }

                            db.insert("menuTable", null, cv);
                        }

                        dbHelper.close();

                    } else {
                        Log.d("Error while parcing", "No data obtainined");
                    }

                } else {

                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                    db.execSQL("DROP TABLE IF EXISTS orderTable");
                    dbHelper.onCreate(db);
                    dbHelper.close();

                 }

           return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressBar.setVisibility(View.GONE);
        }
    }

    public void setBasket() {
        //Checking rows
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT COUNT(*) FROM orderTable", null);

        if (cur != null) {
            cur.moveToFirst();
            fragmentTransaction = fragmentManager.beginTransaction();
            if (cur.getInt(0) != 0) {
                //iconPizza.setImageDrawable(getResources().getDrawable(R.drawable.pizza_icon));
                //iconSushi.setImageDrawable(getResources().getDrawable(R.drawable.sushi_icon));
                //basket.setIcon(R.drawable.basket_icon_focused);
                mPager.setVisibility(View.GONE);
                relativeLayout.setBackgroundResource(R.drawable.background_epon_menu);
                //aboutItem.setVisible(false);
                pizza.setVisible(false);
                //basket.setVisible(false);
                sushi.setVisible(false);
                basket2.setVisible(false);
                mActionBar.setDisplayShowHomeEnabled(false);
                mActionBar.setDisplayShowTitleEnabled(false);
                mActionBar.setDisplayShowCustomEnabled(true);
                mActionBar.setDisplayUseLogoEnabled(false);
                mActionBar.setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM, android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
                mActionBar.setCustomView(actionBarLayout);
                LinearLayout linearLayout=(LinearLayout)mActionBar.getCustomView().findViewById(R.id.layoutCost);
                linearLayout.setOnClickListener(null);
                TextView txtSum=(TextView)mActionBar.getCustomView().findViewById(R.id.txtSum);
                txtSum.setVisibility(View.VISIBLE);
                ImageView imgBasket=(ImageView)mActionBar.getCustomView().findViewById(R.id.imgBasket);
                imgBasket.setVisibility(View.GONE);
                imgBasket.setOnClickListener(null);
                basketFragment = new BasketFragment();
                suCategory = (SubCategory) getSupportFragmentManager().findFragmentByTag("SubCategoryFragment");
                summaryItemFragment = (SummaryItemFragment) getSupportFragmentManager().findFragmentByTag("SummaryItemFragment");

                if (suCategory != null && suCategory.isVisible()){
                    fragmentTransaction.replace(R.id.frgmCont, basketFragment, "BasketFragment");
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                } else if (summaryItemFragment != null && summaryItemFragment.isVisible()){
                    fragmentTransaction.replace(R.id.frgmCont, basketFragment, "BasketFragment");
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                } else {
                    fragmentTransaction.add(R.id.frgmCont, basketFragment, "BasketFragment");
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }

            } else {
                Toast toast = Toast.makeText(this, "Корзина пуста", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    public String getSumm() {

        dbHelper = new DBHelper(this);
        SQLiteDatabase mDataBase = dbHelper.getReadableDatabase();
        Cursor mCursor = mDataBase.query("orderTable", null, null, null, null, null, null);

        double dblSumm = 0;
        double dblprice;

        mCursor.moveToFirst();
        if (!mCursor.isAfterLast()) {
            do {

                String amount = mCursor.getString(mCursor
                        .getColumnIndex("amount"));
                String price = mCursor.getString(mCursor
                        .getColumnIndex("price"));

                try {
                    dblprice = Double.valueOf(price);

                } catch (NumberFormatException nfe) {
                    dblprice = 1;
                }
                double amountInt = Double.valueOf(amount);
                amountInt = dblprice*amountInt;

                dblSumm +=amountInt;

            } while (mCursor.moveToNext());
        }

        //Set Summ
        String summString = String.valueOf(dblSumm);

        return summString;
    }

    public String getRotation(Context context){
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {

            case Surface.ROTATION_0:
                return "portrait";
            case Surface.ROTATION_90:
                return "landscape";
            case Surface.ROTATION_180:
                return "reverse portrait";
            default:
                return "reverse landscape";
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.doubleBackToExitPressedOnce = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //FileCache fileCache = new FileCache(this);
        //fileCache.clear();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAppActiveServiceBound = bindService( new Intent( this, AppActiveService.class ), mAppActiveConnection, Context.BIND_AUTO_CREATE );
    }

    @Override
    protected void onStop() {
        super.onStop();
        if( mAppActiveServiceBound ) {
            unbindService( mAppActiveConnection );
            mAppActiveServiceBound = false;
        }
    }

    public void setCostToActionBar(int cost){
        if (mActionBar.getCustomView()!=null){
            TextView txtPrice= (TextView)mActionBar.getCustomView().findViewById(R.id.priceOrder);
            txtPrice.setText(Integer.toString(cost)+" р.");
        }
    }

    public void setmActionBarTitle(String title){
        if (mActionBar.getCustomView()!=null){
            TextView txtTitle= (TextView)mActionBar.getCustomView().findViewById(R.id.txtTitle);
            txtTitle.setText(title);
        }
    }


    public void setSummaryItemFragment(){
        pizza.setVisible(false);
        //basket.setVisible(false);
        sushi.setVisible(false);
        basket2.setVisible(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(actionBarLayout);
        LinearLayout linearLayout=(LinearLayout)mActionBar.getCustomView().findViewById(R.id.layoutCost);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBasket();
            }
        });
        TextView txtSum=(TextView)mActionBar.getCustomView().findViewById(R.id.txtSum);
        txtSum.setVisibility(View.GONE);
        ImageView imgBasket=(ImageView)mActionBar.getCustomView().findViewById(R.id.imgBasket);
        imgBasket.setVisibility(View.VISIBLE);
        imgBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBasket();
            }
        });
    }




}
