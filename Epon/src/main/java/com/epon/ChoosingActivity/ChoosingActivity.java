package com.epon.ChoosingActivity;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.AppActiveService;
import com.epon.ApiFacade;
import com.epon.DataBase.DBHelper;
import com.epon.EponApp;
import com.epon.MainActivity.MainActivity;
import com.epon.R;
import com.epon.Utils;
import com.epon.lazyList.ConnectionDetector;
import com.epon.network.*;
import com.epon.network.Error;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class ChoosingActivity extends Activity {

    DownloadFileFromURL downloadFileFromURL;

    //Downloading stuff
    //private static String file_url = "http://realty.klim.msk.ru/epon/Base.xls";
    private static String file_url = "http://i.klim.msk.ru/Base.xls";
    final String Base = "BaseEpon.xls";
    double kilobytes;
    int numberOfChange = 0;
    private String ROOT_DIR_NAME = "epongorod";
    //Checking internet connection
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    ProgressBar progressBar;
    DBHelper dbHelper;
    SQLiteDatabase db;
    private boolean mAppActiveServiceBound = false;
    private AppActiveService mAppActiveService = null;
    private static String IMAGES_PATH;
    public static String IMAGES_ITEMS_PATH;
    public static String IMAGES_GROUPS_PATH;
    public static String WORK_DIR_PATH;
    private String SYSTEM_DIR_PATH;
    private TextView txtProgress;
    private ServiceConnection mAppActiveConnection = new ServiceConnection() {
        public void onServiceConnected( ComponentName className, IBinder service ) {
            mAppActiveService = ( (AppActiveService.AppActiveBinder) service ).getService();
        }
        public void onServiceDisconnected( ComponentName className ) {
            mAppActiveService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choosing);

        if ((getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        }
        else if ((getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        else if ((getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setPathes();
        txtProgress=(TextView)findViewById(R.id.txtProgress);
        progressBar = (ProgressBar)findViewById(R.id.progressBarForChoosingPosition);
        dbHelper= new DBHelper(this);
        db= dbHelper.getWritableDatabase();
        startAsyncTask();

    }
    String version="";
    public void startAsyncTask() {
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        progressBar.setVisibility(View.VISIBLE);
        // check for Internet status
        if (isInternetPresent) {
            ApiFacade.getInstance().checkUpdates(new Listener() {
                @Override
                public void onResponse(BaseRequest request) {
                    version = request.getDataString();
                    String oldVersion = EponApp.getInstance().getVersion();
                    if (oldVersion == null || !version.equals(oldVersion)) {
                        ApiFacade.getInstance().getDBFromServer(version, new Listener() {
                                    @Override
                                    public void onResponse(BaseRequest request) {
                                        txtProgress.setVisibility(View.VISIBLE);
                                        DbTask dbTask= new DbTask();
                                        dbTask.execute(request.getDataString());
                                        /*try {
                                            String[] requests = Utils.getSeparatedStrings(";;", request.getDataString());
                                            for (int i = 0; i < requests.length - 1; i++) {
                                                db.execSQL(requests[i]);

                                            }
                                            EponApp.getInstance().setVersion(version);
                                            ApiFacade.getInstance().getImages(new Listener() {
                                                @Override
                                                public void onResponse(BaseRequest request) {
                                                    ImageResponse imageResponse = ((ImageRequest) request).getResponse();
                                                    db.execSQL(DBHelper.DROP_IMAGES);
                                                    db.execSQL(DBHelper.CREATE_IMAGES);
                                                    if (imageResponse.success.size() > 0) {
                                                        for (int i = 0; i < imageResponse.success.size(); i++) {
                                                            ImageFromServer image = imageResponse.success.get(i);
                                                            db.execSQL(dbHelper.getInsertImageString(i + 1, image.type, image.url, image.title));
                                                            int progress=i/(imageResponse.success.size()/100);
                                                            //txtProgress.setText(Integer.toString(progress)+"%");
                                                        }
                                                    }
                                                    startMain();
                                                }
                                                @Override
                                                public void onError(Error error, BaseRequest request) {
                                                    if( error==null||error.code==-1) {
                                                        progressBar.setVisibility(View.GONE);
                                                        showAlertDialog(ChoosingActivity.this, "Внимание!",
                                                                "При отсутствии подключения к интернету цены и меню могут сильно отличаться от действительных", false);
                                                    }
                                                }
                                            });
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }*/
                                    }

                                    @Override
                                    public void onError(Error error, BaseRequest request) {
                                        if( error==null||error.code==-1) {
                                            progressBar.setVisibility(View.GONE);
                                            showAlertDialog(ChoosingActivity.this, "Внимание!",
                                                    "При отсутствии подключения к интернету цены и меню могут сильно отличаться от действительных", false);
                                        }
                                    }
                                }
                        );
                    }else {
                        startMain();
                    }
                }

                    @Override
                    public void onError (com.epon.network.Error error, BaseRequest request){
                        if ( error==null||error.code==-1) {
                            progressBar.setVisibility(View.GONE);
                            showAlertDialog(ChoosingActivity.this, "Внимание!",
                                    "При отсутствии подключения к интернету цены и меню могут сильно отличаться от действительных", false);
                        }
                    }
                }

                );
            /*downloadFileFromURL = new DownloadFileFromURL();
            downloadFileFromURL.execute(file_url);*/

            }else {
            progressBar.setVisibility(View.GONE);
            showAlertDialog(this, "Внимание!",
                    "При отсутствии подключения к интернету цены и меню могут сильно отличаться от действительных", false);
        }

    }

    public class DbTask extends AsyncTask<String,Integer,Void>{


        @Override
        protected Void doInBackground(String... strings) {
            try {
                String[] requests = Utils.getSeparatedStrings(";;", strings[0]);
                for (int i = 0; i < requests.length - 1; i++) {
                    String string1=requests[i].replace("\\\\","\\");
                    string1=string1.replace("\\\"","\"");
                    db.execSQL(string1);
                    int progress=(100*i)/requests.length;
                    publishProgress(progress);

                }
            }catch (Exception e){
                e.printStackTrace();
            }

            return  null;
        }

        protected void onProgressUpdate(Integer... values) {
                txtProgress.setText (getString(R.string.load_menu)+" "+Integer.toString(values[0])+"%");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            EponApp.getInstance().setVersion(version);
            txtProgress.setText (getString(R.string.load_images));
            ApiFacade.getInstance().getImages(new Listener() {
                @Override
                public void onResponse(BaseRequest request) {
                    DbImageTask dbImageTask= new DbImageTask();
                    dbImageTask.execute(((ImageRequest)request).getResponse());
                }

                @Override
                public void onError(Error error, BaseRequest request) {
                    if( error==null||error.code==-1) {
                        progressBar.setVisibility(View.GONE);
                        showAlertDialog(ChoosingActivity.this, "Внимание!",
                                "При отсутствии подключения к интернету цены и меню могут сильно отличаться от действительных", false);
                    }
                }
            });
        }
    }

    public class DbImageTask extends AsyncTask<ImageResponse,Integer,Void>{


        @Override
        protected Void doInBackground(ImageResponse... imageResponses) {
            try {
                ImageResponse imageResponse=imageResponses[0];
                db.execSQL(DBHelper.DROP_IMAGES);
                db.execSQL(DBHelper.CREATE_IMAGES);
                if (imageResponse.success.size() > 0) {
                    for (int i = 0; i < imageResponse.success.size(); i++) {
                        ImageFromServer image = imageResponse.success.get(i);
                        db.execSQL(dbHelper.getInsertImageString(i + 1, image.type, image.url, image.title));
                        int progress=(100*i)/imageResponse.success.size();
                        publishProgress(progress);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            txtProgress.setText(getString(R.string.load_images)+" "+Integer.toString(values[0]) + "%");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            startMain();
        }
    }

    public void  startMain(){
        Intent i = new Intent(ChoosingActivity.this, BannerActivity.class);
        i.putExtra("numberOfChange" , numberOfChange);
        startActivity(i);
        //progressBar.setVisibility(View.GONE);
        finish();
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... f_url) {

            File fileCheck = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath(), Base);

            if (fileCheck.exists()) {
                double bytes = fileCheck.length();
                kilobytes = (bytes / 1024);

                gettingFile();

                File fileCheck1 = new File(Environment.getExternalStorageDirectory()
                        .getAbsolutePath(), Base);

                if (fileCheck1.exists()) {
                    double bytes1 = fileCheck1.length();
                    double kilobytes1 = (bytes1 / 1024);

                    if (kilobytes1 == kilobytes) {
                        numberOfChange = 1;
                    }
                }


            } else if (!fileCheck.exists()) {
                gettingFile();
                numberOfChange = 0;
            }

            return null;
        }


        @Override
        protected void onPostExecute(String file_url) {

            Intent i = new Intent(ChoosingActivity.this, MainActivity.class);
            i.putExtra("numberOfChange" , numberOfChange);
            startActivity(i);
            progressBar.setVisibility(View.GONE);
            finish();

        }

    }

    public void gettingFile() {

        try {
            URL url = new URL(file_url);
            URLConnection connection = url.openConnection();
            connection.connect();

            // Detect the file lenghth
            int fileLength = connection.getContentLength();

            // Locate storage location
            String filepath = Environment.getExternalStorageDirectory()
                    .getPath();

            // Download the file
            InputStream input = new BufferedInputStream(url.openStream());

            // Save the downloaded file
            OutputStream output = new FileOutputStream(filepath + "/" + Base);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                // Publish the progress
                output.write(data, 0, count);
            }

            // Close connection
            output.flush();
            output.close();
            input.close();
        } catch (Exception e) {
            // Error Log
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

       /* try {

            URL url = new URL(file_url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.connect();

            File sdcard = Environment.getExternalStorageDirectory();
            File file = new File(sdcard, Base);

            FileOutputStream fileOutput = new FileOutputStream(file);
            InputStream inputStream = urlConnection.getInputStream();

            byte[] buffer = new byte[1024];
            int bufferLength = 0;

            while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
                fileOutput.write(buffer, 0, bufferLength);
            }
            fileOutput.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    public void showAlertDialog(Activity context, String title, String message, Boolean status) {
        //AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        /*alertDialog.setNegativeButton("Закрыть",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                ChoosingActivity.this.finish();
            }
        });*/

        alertDialog.setPositiveButton("Ок",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                startMain();
                //startAsyncTask();
            }
        });

        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAppActiveServiceBound = bindService( new Intent( this, AppActiveService.class ), mAppActiveConnection, Context.BIND_AUTO_CREATE );
    }

    @Override
    protected void onStop() {
        super.onStop();
        if( mAppActiveServiceBound ) {
            unbindService( mAppActiveConnection );
            mAppActiveServiceBound = false;
        }
    }

    protected void setPathes() {
        SYSTEM_DIR_PATH = getFilesDir().toString();

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            WORK_DIR_PATH = Environment.getExternalStorageDirectory().toString() + File.separator + ROOT_DIR_NAME;
        } else {
            SYSTEM_DIR_PATH = getFilesDir().toString();
            WORK_DIR_PATH = SYSTEM_DIR_PATH + File.separator + ROOT_DIR_NAME;
        }

        IMAGES_PATH = WORK_DIR_PATH + File.separator + "images";
        IMAGES_ITEMS_PATH = IMAGES_PATH + File.separator + "items";
        IMAGES_GROUPS_PATH = getApplicationInfo().dataDir + File.separator + "groups";
        File file = new File(WORK_DIR_PATH);
        if (!file.isDirectory()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        writeNoMediaFile(WORK_DIR_PATH);

        file = new File(IMAGES_ITEMS_PATH);
        if (!file.exists()) {
            file.mkdirs();
        }
        writeNoMediaFile(IMAGES_ITEMS_PATH);
        file = new File(IMAGES_ITEMS_PATH + "/100");
        if (!file.exists()) {
            file.mkdirs();
        }
        writeNoMediaFile(IMAGES_ITEMS_PATH + "/100");
        file = new File(IMAGES_GROUPS_PATH);
        if (!file.exists()) {
            file.mkdirs();
        }
        writeNoMediaFile(IMAGES_GROUPS_PATH);
        file = new File(IMAGES_GROUPS_PATH + "/100");
        if (!file.exists()) {
            file.mkdirs();
        }
        writeNoMediaFile(IMAGES_GROUPS_PATH + "/100");
    }
    private static final String NOMEDIA_FILE = ".nomedia";
    public static void writeNoMediaFile(String imagePath){
        File file = new File(imagePath,NOMEDIA_FILE);
        if (!file.exists()){
            try {
                file.createNewFile();
                Log.i("nomedia", "creation nomedia is succesful for " + imagePath);
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("nomedia", "creation nomedia is failed for " + imagePath);
            }
        }
    }
}
