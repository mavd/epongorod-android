package com.epon.ChoosingActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.epon.DataBase.DBHelper;
import com.epon.EponApp;
import com.epon.MainActivity.MainActivity;
import com.epon.R;
import com.squareup.picasso.Picasso;

public class BannerActivity extends Activity {

    private int numberOfChange;
    private TextView txtNextTimer;
    private TextView txtTitle;
    private TextView txtDescription;
    private ImageView imgPhoto;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private String img_url;
    private String description;
    private String title;
    private CountDownTimer cdTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        numberOfChange = intent.getIntExtra("numberOfChange", 0);

        setContentView(R.layout.activity_banner);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        if ((getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        }
        else if ((getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        else if ((getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        txtNextTimer = (TextView) findViewById(R.id.txtNextTimer);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtDescription = (TextView) findViewById(R.id.txtDescription);
        imgPhoto = (ImageView) findViewById(R.id.imgPhoto);

        dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();

        String[] url_item = {
                "id",
                "title",
                "description",
                "photo"};
        Cursor cursor = db.query("epon_banner",url_item,"is_active = 1",null,null,null,"\"order\"");
        cursor.moveToFirst();
        int count = cursor.getCount();
        if(count > 0) {
            int pos = EponApp.getInstance().getNextBanner();
            if (pos >= count) {
                pos = 0;
            } else {
                cursor.move(pos);
            }
            EponApp.getInstance().setNextBanner(pos + 1);

            title = cursor.getString(1);
            description = cursor.getString(2);
            img_url = "http://epongorod.mobissa.ru/uploads/banner/"
                    + cursor.getString(0)
                    + "/"
                    + cursor.getString(3);
        }

        try {
            Picasso.with(BannerActivity.this).load(img_url).fit().centerCrop().into(imgPhoto);
            txtTitle.setText(title);
            txtDescription.setText(description);
        } catch (Exception e){
            e.printStackTrace();
        }

        cdTimer = new CountDownTimer(5500, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.d("TIMER","time = " + millisUntilFinished);
                txtNextTimer.setText(String.format(getResources().getString(R.string.banner_next), millisUntilFinished / 1000));
            }

            public void onFinish() {
                Log.d("TIMER","fin");
                txtNextTimer.setText(null);
                Intent i = new Intent(BannerActivity.this, MainActivity.class);
                i.putExtra("numberOfChange" , numberOfChange);
                startActivity(i);
                finish();
            }
        };
        cdTimer.start();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(cdTimer != null) {
            cdTimer.cancel();
            cdTimer.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        cdTimer.cancel();
    }

}
