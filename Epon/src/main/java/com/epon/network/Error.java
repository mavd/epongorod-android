package com.epon.network;

/**
 * Created by Denis Zabozhanov on 06/02/14.
 */
public class Error {
    public int code;
    public String message;
    public static   final int TOKEN_NOT_FOUND=4;

    public Error(String message, int code) {
        this.message = message;
        this.code = code;
    }
}
