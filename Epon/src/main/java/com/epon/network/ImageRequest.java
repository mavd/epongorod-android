package com.epon.network;

import com.google.gson.JsonParseException;

import org.json.JSONException;

/**
 * Created by macbook on 28/12/14.
 */
public class ImageRequest extends BaseRequest {
    public static final  String api = "getImages";
    ImageResponse response;
    public ImageRequest(Listener listener) {
        super(Method.GET, api, listener);
    }
    public static final  String CATEGORY_TYPE="category";
    public static final  String PRODUCT_TYPE="product";



    @Override
    protected void parseResponse() throws JSONException {
        try {
            response = mGson.fromJson(getmResponse(),ImageResponse.class);
            notifySuccess();
        }catch (JsonParseException e){
            e.printStackTrace();
        }

    }

    public ImageResponse getResponse() {
        return response;
    }
}
