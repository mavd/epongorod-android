package com.epon.network;

/**
 * Created by zabozhanov on 25/01/14.
 */
public abstract class Listener {
	public abstract void onResponse(BaseRequest request);
	public abstract void onError(Error error, BaseRequest request);
}
