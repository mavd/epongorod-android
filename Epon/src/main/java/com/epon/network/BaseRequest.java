package com.epon.network;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.epon.EponApp;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by zabozhanov on 25/01/14.
 */
public abstract class BaseRequest extends Request<String> {
    public final static String REQUEST_ERROR = "REQUEST_ERROR";
    public final static String REQUEST_DATA = "REQUEST_DATA";
    public final static String TOKEN_NOT_FOUND="TOKEN_NOT_FOUND";
    public final static String TIME_OUT_ERROR="TIME_OUT_ERROR";
    public final static String SUCCES_REQUEST="SUCCES_REQUEST";
    protected final static String API_KEY = "111";
    protected final static String SERVER_URL =EponApp.SERVER_URL;
    private Map<String, String> mParams;
    protected Gson mGson;
    private String mApiMethod;
    private Status mStatus;
    public Status getStatus() {
        return mStatus;
    }

    private String mData;

    public String getDataString() {
        return mData;
    }

    public String getApiMethod() {
        return mApiMethod;
    }

    protected Listener mListener;

    protected Error mError;

    public Error getError() {
        return mError;
    }
    private String mResponse;

    public BaseRequest(int method, String apiMethod, Listener listener) {
        super(method, SERVER_URL + apiMethod, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(com.android.volley.Error error) {
                Intent intent = new Intent(REQUEST_ERROR);
                intent.putExtra("data", error.toString());
               /* LocalBroadcastManager
                        .getInstance(TaxiApp.getInstance()
                                .getApplicationContext()).sendBroadcast(intent);*/
            }
        });
        this.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mParams = new HashMap<String, String>();
        mGson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PRIVATE).create();
        mApiMethod = apiMethod;
        mListener = listener;
    }

    protected Context getContext() {
        return EponApp.getInstance().getApplicationContext();
    }

    @Override
    public void deliverError(com.android.volley.Error error) {
        super.deliverError(error);
        if (error != null) {
            notifyFailure(new Error(error.getMessage(), -1));
        } else {
            notifyFailure(new Error("Неизвестная ошибка", -1));
        }
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }
        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(String response)  {
        try {
            JSONObject jsonObject = new JSONObject(response);
            mResponse = response;
            JSONArray messageArray=jsonObject.getJSONArray("messages");
            Message message= new Message();
            if (!messageArray.isNull(0)) {
                message = mGson.fromJson(messageArray.get(0).toString(), Message.class);
            }
            mError = new com.epon.network.Error("", 0);
            if (message.type.equals("danger")){
                mError= new com.epon.network.Error(message.text,1);
                notifyFailure(mError);
            }else {
                try {

                    JSONArray successArray = jsonObject.getJSONArray("success");
                    if (!successArray.isNull(0)) {
                        mData = successArray.get(0).toString();
                        parseResponse();
                    } else {
                        requestFailure(mError);
                    }
                }catch (JSONException e){
                    mData=jsonObject.getString("success");
                    parseResponse();
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
            requestFailure(null);
        }
    }
    public void errorAction(int errorCode){
        switch (errorCode){
            case Error.TOKEN_NOT_FOUND:{
                Intent intent= new Intent(BaseRequest.TOKEN_NOT_FOUND);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
            }
        }

    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        //mParams.put("api_key", API_KEY);
        return mParams;
    }

    @Override
    public String getUrl() {
        if (this.getMethod() == Method.GET) {
            try {
                String url = super.getUrl() + "&";
                for (String key : getParams().keySet()) {
                    url = url + key + "=" + getParams().get(key) + "&";
                }
                return url;
            } catch (AuthFailureError ex) {
                ex.printStackTrace();
            }
        }
        return super.getUrl();
    }

    protected abstract void parseResponse() throws JSONException;

    protected void requestFailure(Error error) {
        notifyFailure(error);
    }

    protected void notifySuccess() {
        if (mListener != null) {
            mListener.onResponse(this);
        }
    }

    protected void notifyFailure(Error error) {
        mError = error;

        if (mListener != null) {
            mListener.onError(error, this);
            if  (mError!=null) {
                if (mError.code == 1) {
                    EponApp.getInstance().showToast(mError.message);
                }
            }
        }
    }
    public void sentError() {
        if (this.mError == null) {
            sentSucces();
        } else {
            try {
                if (!this.mError.message.contains("java")) { //todo: был крэш!
                    sentSucces();
                } else {
                    Intent intent = new Intent(TIME_OUT_ERROR);
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                }
            } catch (Exception ex) {

                 ex.printStackTrace();
            }
        }
    }
    public void sentSucces(){
        Intent intent= new Intent(SUCCES_REQUEST);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }


    protected void addParam(String name, Object value) {
        mParams.put(name, String.valueOf(value));
    }

    /**
     * Метод для тестов api без запроса к серверу
     */
    public void execute() {
    }

    public String getmResponse() {
        return mResponse;
    }
}
