package com.epon.network;

import com.epon.EponApp;

import org.json.JSONException;

/**
 * Created by macbook on 28/12/14.
 */
public class CheckRequest extends BaseRequest {
    public static final  String api = "checkUpdates";
    public CheckRequest(Listener listener) {
        super(Method.GET, api, listener);
    }

    @Override
    protected void parseResponse() throws JSONException {
        notifySuccess();

    }
}
