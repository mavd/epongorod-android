package com.epon.network;

import org.json.JSONException;

/**
 * Created by macbook on 28/12/14.
 */
public class SqlRequest extends BaseRequest {
    public static final  String api = "getSqLDump";
    public SqlRequest(String version, Listener listener) {
        super(Method.GET, api, listener);
        addParam("latestVersion",version);
    }

    @Override
    protected void parseResponse() throws JSONException {

        notifySuccess();
    }
}
