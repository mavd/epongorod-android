package com.epon.network;

import org.json.JSONException;

/**
 * Created by macbook on 28/12/14.
 */
public class OrderRequest extends BaseRequest {
    public static final  String api = "createOrderNew";
    public OrderRequest(String order, Listener listener) {
        super(Method.POST, api, listener);
        addParam("action","createOrderNew");
        addParam("orderData",order);
    }

    @Override
    protected void parseResponse() throws JSONException {
        notifySuccess();
    }
}
