package com.epon.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.Util;
import com.epon.ChoosingActivity.ChoosingActivity;
import com.epon.DataBase.Category;
import com.epon.DataBase.Product;
import com.epon.DataBase.helpers.AllHelper;
import com.epon.DataBase.helpers.ImageHelper;
import com.epon.DataBase.helpers.ProductHelper;
import com.epon.EponApp;
import com.epon.R;
import com.epon.Utils;
import com.epon.custom.ImageSetter;
import com.epon.lazyList.ImageLoader;
import com.epon.network.ImageRequest;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by macbook on 03/01/15.
 */
public class CategoryAdapter extends BaseAdapter {
    Context context;
    ArrayList<Category> categories;
    LayoutInflater inflater;
    private ImageLoader imageLoader;
    ImageHelper imageHelper;
    int orientation;
    ProductHelper productHelper;
    private int tag_id;
    ImageSetter imageSetter;
    public CategoryAdapter(Context context,ArrayList<Category> list,int orientation,int tag_id){
        this.context= context;
        this.categories= list;
        inflater= LayoutInflater.from(context);
        imageLoader = new ImageLoader(context.getApplicationContext());
        imageHelper= EponApp.getInstance().getAllHelper().getImageHelper();
        this.orientation=orientation;
        productHelper=EponApp.getInstance().getAllHelper().getProductHelper();
        this.tag_id=tag_id;
        imageSetter= new ImageSetter();
    }
    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v= null;
        Category category= categories.get(i);
        if (orientation==EponApp.PORTRAIT) {
            v = inflater.inflate(R.layout.element_list_sushi, null);
            ArrayList<Product> products=productHelper.getProductsByTag(tag_id,category.id);
            TextView txtCount=(TextView)v.findViewById(R.id.txtDesc);
            txtCount.setText(Integer.toString(products.size())+" " +context.getString(R.string.dish)+ Utils.getDeclension(products.size()));
            txtCount.setVisibility(View.VISIBLE);
            FrameLayout imgFrame=(FrameLayout)v.findViewById(R.id.frameLay);
            imgFrame.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.circle));
        }else {
            v = inflater.inflate(R.layout.element_grid, null);
        }
        TextView title = (TextView) v.findViewById(R.id.nameOfCafe);
        TextView id = (TextView) v.findViewById(R.id.idOfSushiMenu);
        ImageView logoImageView=(ImageView)v.findViewById(R.id.logoImageView);
        title.setText(category.title.replace("\\",""));
        id.setText(Integer.toString(category.id));
        String url=imageHelper.getUrlByTitleAndCategory(ImageRequest.CATEGORY_TYPE, category.photo);
        String path= ChoosingActivity.IMAGES_GROUPS_PATH+ File.separator+category.photo;
        //imageSetter.downloadWithoutAnimation(path,url,logoImageView,context.getResources().getDimensionPixelSize(R.dimen.category_img_size));
        imageLoader.DisplayImage(imageHelper.getUrlByTitleAndCategory(ImageRequest.CATEGORY_TYPE,category.photo),logoImageView,true,context.getResources().getDimensionPixelSize(R.dimen.category_img_size));
        //com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(imageHelper.getUrlByTitleAndCategory(ImageRequest.CATEGORY_TYPE,category.photo),
          //      logoImageView);
        return v;
    }
}
