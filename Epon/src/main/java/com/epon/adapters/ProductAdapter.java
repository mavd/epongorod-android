package com.epon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.epon.DataBase.Category;
import com.epon.DataBase.Product;
import com.epon.DataBase.helpers.ImageHelper;
import com.epon.EponApp;
import com.epon.R;
import com.epon.lazyList.ImageLoader;
import com.epon.network.ImageRequest;

import java.util.ArrayList;

/**
 * Created by macbook on 03/01/15.
 */
public class ProductAdapter extends BaseAdapter {
    Context context;
    ArrayList<Product> products;
    LayoutInflater inflater;
    private ImageLoader imageLoader;
    ImageHelper imageHelper;
    int orientation;
    public ProductAdapter(Context context,ArrayList<Product> list,int orientation){
        this.context= context;
        this.products = list;
        inflater= LayoutInflater.from(context);
        imageLoader = new ImageLoader(context.getApplicationContext());
        imageHelper= EponApp.getInstance().getAllHelper().getImageHelper();
        this.orientation=orientation;
    }
    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int i) {
        return products.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v= null;
        Product product= products.get(i);
        if (orientation==EponApp.PORTRAIT) {
            v = inflater.inflate(R.layout.element_list_sushi, null);
            TextView desc=(TextView)v.findViewById(R.id.txtDesc);
            TextView cost=(TextView)v.findViewById(R.id.txtCost);
            desc.setVisibility(View.VISIBLE);
            cost.setVisibility(View.VISIBLE);
            desc.setText(product.description.replace("\\",""));
            cost.setText(Integer.toString(product.cost)+" р.");
        }else {
            v = inflater.inflate(R.layout.element_grid, null);
        }
        TextView title = (TextView) v.findViewById(R.id.nameOfCafe);
        TextView id = (TextView) v.findViewById(R.id.idOfSushiMenu);
        ImageView logoImageView=(ImageView)v.findViewById(R.id.logoImageView);

        title.setText(product.title.replace("\\\\","\\"));
        id.setText(Integer.toString(product.id));
        imageLoader.DisplayImage(imageHelper.getUrlByTitleAndCategory(ImageRequest.PRODUCT_TYPE,product.photo),logoImageView,false,
                context.getResources().getDimensionPixelSize(R.dimen.category_img_size));
        //com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(imageHelper.getUrlByTitleAndCategory(ImageRequest.PRODUCT_TYPE,product.photo),logoImageView);
        return v;
    }
}
