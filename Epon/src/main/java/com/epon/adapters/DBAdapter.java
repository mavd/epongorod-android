package com.epon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.epon.DataBase.MyData;
import com.epon.R;
import com.epon.lazyList.ImageLoader;

import java.util.ArrayList;

public class DBAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private ArrayList<MyData> arrayMyData;
    private ImageLoader imageLoader;
    private Context context;

    public DBAdapter (Context ctx, ArrayList<MyData> arr) {

        mLayoutInflater = LayoutInflater.from(ctx);
        imageLoader = new ImageLoader(ctx.getApplicationContext());
        setArrayMyData(arr);
        context=ctx;
    }

    public ArrayList<MyData> getArrayMyData() {
        return arrayMyData;
    }

    public void setArrayMyData(ArrayList<MyData> arrayMyData) {
        this.arrayMyData = arrayMyData;
    }

    public int getCount () {
        return arrayMyData.size();
    }

    public Object getItem (int position) {

        return position;
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null)
            convertView = mLayoutInflater.inflate(R.layout.element_list_sushi, null);

        TextView title = (TextView) convertView.findViewById(R.id.nameOfCafe);
        TextView id = (TextView) convertView.findViewById(R.id.idOfSushiMenu);
        ImageView logoImageView=(ImageView)convertView.findViewById(R.id.logoImageView);


        MyData md = arrayMyData.get(position);
        title.setText(md.getSubcategory());
        String id1 = String.valueOf(md.getId());
        id.setText(id1);

        String nameOf = title.getText().toString();

        if (nameOf.equalsIgnoreCase("Суши")) {
            logoImageView.setImageResource(R.drawable.sushi);
           // amount.setText("9 видов");
        } else if(nameOf.equalsIgnoreCase("Роллы")) {
            logoImageView.setImageResource(R.drawable.rolls);
        } else if(nameOf.equalsIgnoreCase("Гунканы")) {
            logoImageView.setImageResource(R.drawable.gunkan);
        }
        else if(nameOf.equalsIgnoreCase("Салаты")) {
            logoImageView.setImageResource(R.drawable.salst);
        }
        else if(nameOf.equalsIgnoreCase("Горячие закуски")) {
            logoImageView.setImageResource(R.drawable.hot_snack);
        }
        else if(nameOf.equalsIgnoreCase("Супы")) {
            logoImageView.setImageResource(R.drawable.soup);
        }
        else if(nameOf.equalsIgnoreCase("Горячие блюда")) {
            logoImageView.setImageResource(R.drawable.hot_dishes);
        }
        else if(nameOf.equalsIgnoreCase("Лапша 'Удон'")) {
            logoImageView.setImageResource(R.drawable.udon);
        }

        else {

            imageLoader.DisplayImage(md.getUrl(), logoImageView,true,context.getResources().getDimensionPixelSize(R.dimen.category_img_size));
        }

        return convertView;
    }
}