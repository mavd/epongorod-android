package com.epon.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.epon.R;
import com.epon.fragments.ListFragmentSushi;
import com.epon.lazyList.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;

public class SushiAdapter extends BaseAdapter {

    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater = null;
    private ImageLoader imageLoader;
    private Activity activity;

    public SushiAdapter(Activity a, ArrayList<HashMap<String, String>> d) {

        activity = a;

        data = d;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;

        if(convertView == null)
            vi = inflater.inflate(R.layout.element_list_sushi, null);

        TextView title = (TextView) vi.findViewById(R.id.nameOfCafe);
        TextView amount = (TextView) vi.findViewById(R.id.amount);
        ImageView logoImageView=(ImageView)vi.findViewById(R.id.logoImageView);

        HashMap<String, String> cafe;
        cafe = data.get(position);

        // Setting all values in listview
        title.setText(cafe.get(ListFragmentSushi.KEY_NAME));
        String nameOf = title.getText().toString();

        if (nameOf.equalsIgnoreCase("Суши")) {
            logoImageView.setImageResource(R.drawable.sushi);
            amount.setText("9 видов");
        } else if(nameOf.equalsIgnoreCase("Роллы")) {
            logoImageView.setImageResource(R.drawable.rolls);
            amount.setText("21 вид");
        } else if(nameOf.equalsIgnoreCase("Гунканы")) {
            logoImageView.setImageResource(R.drawable.gunkan);
            amount.setText("10 видов");
        }
        else if(nameOf.equalsIgnoreCase("Салаты")) {
            logoImageView.setImageResource(R.drawable.salst);
            amount.setText("9 видов");
        }
        else if(nameOf.equalsIgnoreCase("Горячие закуски")) {
            logoImageView.setImageResource(R.drawable.hot_snack);
            amount.setText("8 видов");
        }
        else if(nameOf.equalsIgnoreCase("Супы")) {
            logoImageView.setImageResource(R.drawable.soup);
            amount.setText("5 видов");
        }
        else if(nameOf.equalsIgnoreCase("Горячие блюда")) {
            logoImageView.setImageResource(R.drawable.hot_dishes);
            amount.setText("7 видов");
        }
        else if(nameOf.equalsIgnoreCase("Лапша 'Удон'")) {
            logoImageView.setImageResource(R.drawable.udon);
            amount.setText("1 вид");
        }
        else {
            imageLoader.DisplayImage(cafe.get(ListFragmentSushi.KEY_LOGO), logoImageView,true,activity.getResources().getDimensionPixelSize(R.dimen.category_img_size));
        }

        return vi;
    }
}