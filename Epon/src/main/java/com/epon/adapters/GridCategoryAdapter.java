package com.epon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.epon.DataBase.Category;
import com.epon.DataBase.helpers.ImageHelper;
import com.epon.EponApp;
import com.epon.R;
import com.epon.lazyList.ImageLoader;
import com.epon.network.ImageRequest;

import java.util.ArrayList;

/**
 * Created by macbook on 03/01/15.
 */
public class GridCategoryAdapter extends BaseAdapter {
    Context context;
    ArrayList<Category> categories;
    LayoutInflater inflater;
    private ImageLoader imageLoader;
    ImageHelper imageHelper;
    public GridCategoryAdapter(Context context,ArrayList<Category> list){
        this.context= context;
        this.categories= list;
        inflater= LayoutInflater.from(context);
        imageLoader = new ImageLoader(context.getApplicationContext());
        imageHelper= EponApp.getInstance().getAllHelper().getImageHelper();
    }
    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v= inflater.inflate(R.layout.element_list_sushi,null);
        TextView title = (TextView) v.findViewById(R.id.nameOfCafe);
        TextView id = (TextView) v.findViewById(R.id.idOfSushiMenu);
        ImageView logoImageView=(ImageView)v.findViewById(R.id.logoImageView);
        Category category= categories.get(i);
        title.setText(category.title);
        id.setText(Integer.toString(category.id));
        imageLoader.DisplayImage(imageHelper.getUrlByTitleAndCategory(ImageRequest.CATEGORY_TYPE,category.photo),logoImageView,true,
                context.getResources().getDimensionPixelSize(R.dimen.category_img_size));
        return v;
    }
}
