package com.epon.adapters;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.epon.DataBase.BasketData;
import com.epon.DataBase.DBHelper;
import com.epon.DataBase.helpers.ImageHelper;
import com.epon.EponApp;
import com.epon.R;
import com.epon.lazyList.ImageLoader;
import com.epon.network.ImageRequest;

import java.util.ArrayList;

public class DBBasketAdapter extends BaseAdapter{

    private LayoutInflater mLayoutInflater;
    private ArrayList<BasketData> arrayMyData;
    private ImageLoader imageLoader;
    private Context mContext;

    //Values
    private String idString;

    //DB
    private DBHelper dbHelper;
    private SQLiteDatabase mDataBase;
    ImageHelper imageHelper;

    public DBBasketAdapter(Context ctx, ArrayList<BasketData> arr) {
        mLayoutInflater = LayoutInflater.from(ctx);
        imageLoader = new ImageLoader(ctx.getApplicationContext());
        imageHelper= EponApp.getInstance().getAllHelper().getImageHelper();
        setArrayMyData(arr);
        this.mContext=ctx;
    }

    public ArrayList<BasketData> getArrayMyData() {
        return arrayMyData;
    }

    public void setArrayMyData(ArrayList<BasketData> arrayMyData) {
        this.arrayMyData = arrayMyData;
    }

    public int getCount () {
        return arrayMyData.size();
    }

    public Object getItem (int position) {

        return position;
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null)
            convertView = mLayoutInflater.inflate(R.layout.element_list_basket, null);

        TextView title = (TextView) convertView.findViewById(R.id.nameOfCafe);
        TextView amount = (TextView) convertView.findViewById(R.id.amount);
        TextView price = (TextView) convertView.findViewById(R.id.priceBasket);
        ImageView logoImageView=(ImageView)convertView.findViewById(R.id.logoImageView);
        TextView id = (TextView) convertView.findViewById(R.id.idOfSushiMenu);
        TextView idMenu = (TextView) convertView.findViewById(R.id.idOfMenu);
        TextView txtDesc=(TextView)convertView.findViewById(R.id.txtDesc);

        BasketData md = arrayMyData.get(position);

        String sdafgag = String.valueOf(md.getMenuId());
        idMenu.setText(sdafgag);
        txtDesc.setText(md.getDesc().replace("\\",""));
        title.setText(md.getName());
        amount.setText(md.getAmount() + "x");
        idString = String.valueOf(md.getId());
        id.setText(idString);

        String priceString = md.getSumm();
        if (priceString != null) {
            price.setText(priceString + "р");
        }

        String nameOf = title.getText().toString();

        if (nameOf.equalsIgnoreCase("Суши")) {
            logoImageView.setImageResource(R.drawable.sushi);
            // amount.setText("9 видов");
        } else if(nameOf.equalsIgnoreCase("Роллы")) {
            logoImageView.setImageResource(R.drawable.rolls);
        } else if(nameOf.equalsIgnoreCase("Гунканы")) {
            logoImageView.setImageResource(R.drawable.gunkan);
        }
        else if(nameOf.equalsIgnoreCase("Салаты")) {
            logoImageView.setImageResource(R.drawable.salst);
        }
        else if(nameOf.equalsIgnoreCase("Горячие закуски")) {
            logoImageView.setImageResource(R.drawable.hot_snack);
        }
        else if(nameOf.equalsIgnoreCase("Супы")) {
            logoImageView.setImageResource(R.drawable.soup);
        }
        else if(nameOf.equalsIgnoreCase("Горячие блюда")) {
            logoImageView.setImageResource(R.drawable.hot_dishes);
        }
        else if(nameOf.equalsIgnoreCase("Лапша 'Удон'")) {
            logoImageView.setImageResource(R.drawable.udon);
        }

        else {
            imageLoader.DisplayImage(imageHelper.getUrlByTitleAndCategory(ImageRequest.PRODUCT_TYPE, md.getUrl()),logoImageView,false,
                    mContext.getResources().getDimensionPixelSize(R.dimen.category_img_size));
        }
        return convertView;
    }


    /*

    dbHelper = new DBHelper(mContext);
                ContentValues cv = new ContentValues();
                mDataBase = dbHelper.getWritableDatabase();

                String getValue = String.valueOf(np.getValue());
                String idof = "_id = " + idString;

                cv.put("amount", getValue);
                mDataBase.update("orderTable", cv, idof, null);
                mDataBase.close();

    * */

}