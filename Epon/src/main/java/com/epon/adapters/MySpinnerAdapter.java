package com.epon.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.epon.R;

import java.util.List;

/**
 * Created by macbook on 28/12/14.
 */
public class MySpinnerAdapter extends ArrayAdapter<String>
{
    Context context;
    List<String> items;
    public MySpinnerAdapter(final Context context,
                            final int textViewResourceId, List<String> vendor_name) {
        super(context, textViewResourceId, vendor_name);
        this.items = vendor_name;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.spinner_row, parent, false);
        }
        // android.R.id.text1 is default text view in resource of the android.
        // android.R.layout.simple_spinner_item is default layout in resources of android.
        TextView tv = (TextView) convertView.findViewById(R.id.spinnerText);
        tv.setText(items.get(position));
        tv.setTextColor(Color.BLACK);
        //tv.setTextSize(12);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.spiner_dropdown_item, parent, false);
        }
        // android.R.id.text1 is default text view in resource of the android.
        // android.R.layout.simple_spinner_item is default layout in resources of android.
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        tv.setText(items.get(position));
        tv.setTextColor(Color.WHITE);
        //tv.setTextSize(15);
        tv.setSingleLine(false);
        return convertView;
    }
}
