/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.epon.custom;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This helper class download images from the Internet and binds those with the provided ImageView.
 *
 * <p>It requires the INTERNET permission, which should be added to your application's manifest
 * file.</p>
 *
 * A local cache of downloaded images is maintained internally to improve performance.
 */
public class ImageSetter {
	
	private static ImageSetter instance;
	
    private static final String LOG_TAG = "ImageSetter";

	private int mRequiredSize;
    
//    private ImageSetter(){
//    	
//    }
//    
//    public static ImageSetter getInstance(){
//		if(instance == null)
//			synchronized (ImageSetter.class) {
//				if(instance == null) {
//					instance = new ImageSetter();
//				}	
//			}
//		return instance;
//    }
    
    /**
     * Download the specified image from the Internet and binds it to the provided ImageView. The
     * binding is immediate if the image is found in the cache and will be done asynchronously
     * otherwise. A null bitmap will be associated to the ImageView if an error occurs.
     *
     * @param path The URL of the image to download.
     * @param imageView The ImageView to bind the downloaded image to.
     */
	
	
	
	

	private Context mContext;
	
	AlphaAnimation animationFadeIn;
	
    public void download(String path, ImageView imageView, int requiredSize) {
//        resetPurgeTimer();
        Bitmap bitmap = getBitmapFromCache(path);
        mContext = imageView.getContext();
//        imageView.setAlpha(0);
//        animationFadeIn = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        
        animationFadeIn = new AlphaAnimation(0f,1f);
        animationFadeIn.setDuration(500);
        
        mRequiredSize = requiredSize;
//        forceSet(path, imageView);
        if (bitmap == null) {
            forceSet(path, imageView);
        } else {
            cancelPotentialTask(path, imageView);
            imageView.setImageBitmap(bitmap);
//            imageView.setAlpha(255);
            imageView.setAnimation(animationFadeIn);
        }
    }

    public void  downloadWithoutAnimation(String path, String url, ImageView imageView, int requiredSize){
        //      resetPurgeTimer();
        Bitmap bitmap = getBitmapFromCache(path);
        mContext = imageView.getContext();
        mRequiredSize = requiredSize;

        if (bitmap == null) {
            forceSet(path, url, imageView);
        } else {
            cancelPotentialTask(path, imageView);
            imageView.setImageBitmap(bitmap);
        }
    }
    public void download(String path, String url, ImageView imageView, int requiredSize) {
//      resetPurgeTimer();
      Bitmap bitmap = getBitmapFromCache(path);
      mContext = imageView.getContext();
//      imageView.setAlpha(0);
//      animationFadeIn = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
      
      animationFadeIn = new AlphaAnimation(0f,1f);
      animationFadeIn.setDuration(500);
      
      mRequiredSize = requiredSize;
//      forceSet(path, imageView);
      if (bitmap == null) {
          forceSet(path, url, imageView);
      } else {
          cancelPotentialTask(path, imageView);
          if (imageView!=null) {
              imageView.setImageBitmap(bitmap);
//          imageView.setAlpha(255);
              imageView.setAnimation(animationFadeIn);
          }
      }
  }

    /*
     * Same as download but the image is always downloaded and the cache is not used.
     * Kept private at the moment as its interest is not clear.
       private void forceDownload(String url, ImageView view) {
          forceDownload(url, view, null);
       }
     */

    /**
     * Same as download but the image is always downloaded and the cache is not used.
     * Kept private at the moment as its interest is not clear.
     */
    private void forceSet(String path, ImageView imageView) {
        // State sanity: url is guaranteed to never be null in DownloadedDrawable and cache keys.
        if (path == null) {
            imageView.setImageDrawable(null);
            return;
        }

        if (cancelPotentialTask(path, imageView)) {
        	BitmapTask task = new BitmapTask(imageView);
            DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task);
            imageView.setImageDrawable(downloadedDrawable);
            imageView.setMinimumHeight(156);
            task.execute(path);
        }
    }
    
    private void forceSet(String path, String url, ImageView imageView) {
        // State sanity: url is guaranteed to never be null in DownloadedDrawable and cache keys.
        if (path == null) {
            imageView.setImageDrawable(null);
            return;
        }

        if (cancelPotentialTask(path, imageView)) {
        	BitmapTask task = new BitmapTask(imageView);
            DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task);
            imageView.setImageDrawable(downloadedDrawable);
            imageView.setMinimumHeight(156);
            task.url = url;
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB) {
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,path);
            }else{
                task.execute(path);
            }

        }
    }

    /**
     * Returns true if the current download has been canceled or if there was no download in
     * progress on this image view.
     * Returns false if the download in progress deals with the same url. The download is not
     * stopped in that case.
     */
    private static boolean cancelPotentialTask(String url, ImageView imageView) {
        BitmapTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);

        if (bitmapDownloaderTask != null) {
            String bitmapUrl = bitmapDownloaderTask.path;
            if ((bitmapUrl == null) || (!bitmapUrl.equals(url))) {
                bitmapDownloaderTask.cancel(true);
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * @param imageView Any imageView
     * @return Retrieve the currently active download task (if any) associated with this imageView.
     * null if there is no such task.
     */
    private static BitmapTask getBitmapDownloaderTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable) {
                DownloadedDrawable downloadedDrawable = (DownloadedDrawable)drawable;
                return downloadedDrawable.getBitmapDownloaderTask();
            }
        }
        return null;
    }

    Bitmap decodeBitmapFromFilesystem(String path) {
    	Bitmap bitmap = BitmapFactory.decodeFile(path);
    	if(mRequiredSize != 0){
    		bitmap = ImageResizer.resize(path, mRequiredSize);
    	}
		return bitmap;
    }
    
    Bitmap decodeBitmapFromUrl(String url, String filePath) {
    	Bitmap bitmap = null;

		try{
		    URL ulrn = new URL(url);
		    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
		    InputStream is =  new BufferedInputStream(con.getInputStream());
		    FileOutputStream fos;
//		    Bitmap bmp = BitmapFactory.decodeStream(is);
//		    if (null != bmp)
//		    	bitmap = bmp;
//		    else
//		        System.out.println("The Bitmap is NULL");
		    
		    fos = new FileOutputStream(filePath);
        	
        	byte[] buffer = new byte[50*1024];
        	int character = -1;
        	while ((character = is.read(buffer)) != -1) {
        		fos.write(buffer, 0, character);
        	}
        	
        	if (is != null)
        		is.close();
        	if (fos != null)
        		fos.close();
        	bitmap = decodeBitmapFromFilesystem(filePath);

		} catch(Exception e) {
			Log.i("decodeexc", "decode failed!");
		}
		
		return bitmap;
    }

    /*
     * An InputStream that skips the exact number of bytes provided, unless it reaches EOF.
     */
    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break;  // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    /**
     * The actual AsyncTask that will asynchronously download the image.
     */
    public  class BitmapTask extends AsyncTask<String, Void, Bitmap> {
        private String path;
        public String url ="";
        private final WeakReference<ImageView> imageViewReference;

        public BitmapTask(ImageView imageView) {
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        /**
         * Actual download method.
         */
        @Override
        protected Bitmap doInBackground(String... params) {
            path = params[0];
           
            Bitmap bitmapFromFS = decodeBitmapFromFilesystem(path);
            if(bitmapFromFS != null)
            	return bitmapFromFS;
            else 
            	return decodeBitmapFromUrl(url, path);
//            return decodeBitmapFromFilesystem(url);
        }

        /**
         * Once the image is downloaded, associates it to the imageView
         */
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            onPostExecuteMethod(bitmap);
        }

		public void onPostExecuteMethod(Bitmap bitmap) {
			addBitmapToCache(path, bitmap);

            if (imageViewReference != null) {
                ImageView imageView = imageViewReference.get();
                BitmapTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
                // Change bitmap only if this process is still associated with it
                // Or if we don't use any bitmap to task association (NO_DOWNLOADED_DRAWABLE mode)
                if (this == bitmapDownloaderTask) {
                    imageView.setImageBitmap(bitmap);
                    imageView.setAnimation(animationFadeIn);
//                    imageView.setAlpha(255);
                }
            }
		}
    }
    
    
    

    /**
     * A fake Drawable that will be attached to the imageView while the download is in progress.
     *
     * <p>Contains a reference to the actual download task, so that a download task can be stopped
     * if a new binding is required, and makes sure that only the last started download process can
     * bind its result, independently of the download finish order.</p>
     */
    static class DownloadedDrawable extends ColorDrawable {
        private final WeakReference<BitmapTask> bitmapDownloaderTaskReference;

        public DownloadedDrawable(BitmapTask bitmapDownloaderTask) {
//            super(Color.WHITE);
            bitmapDownloaderTaskReference = new WeakReference<BitmapTask>(bitmapDownloaderTask);
        }

        public BitmapTask getBitmapDownloaderTask() {
            return bitmapDownloaderTaskReference.get();
        }
    }

//    public void setMode(Mode mode) {
//        this.mode = mode;
//        clearCache();
//    }

    
    /*
     * Cache-related fields and methods.
     * 
     * We use a hard and a soft cache. A soft reference cache is too aggressively cleared by the
     * Garbage Collector.
     */
    
    private static final int HARD_CACHE_CAPACITY = 10;
    private static final int DELAY_BEFORE_PURGE = 10 * 1000; // in milliseconds

    // Hard cache, with a fixed maximum capacity and a life duration
    private final HashMap<String, Bitmap> sHardBitmapCache =
        new LinkedHashMap<String, Bitmap>(HARD_CACHE_CAPACITY / 2, 0.75f, true) {
        @Override
        protected boolean removeEldestEntry(LinkedHashMap.Entry<String, Bitmap> eldest) {
            if (size() > HARD_CACHE_CAPACITY) {
                // Entries push-out of hard reference cache are transferred to soft reference cache
                sSoftBitmapCache.put(eldest.getKey(), new SoftReference<Bitmap>(eldest.getValue()));
                return true;
            } else
                return false;
        }
    };

    // Soft cache for bitmaps kicked out of hard cache
    private final static ConcurrentHashMap<String, SoftReference<Bitmap>> sSoftBitmapCache =
        new ConcurrentHashMap<String, SoftReference<Bitmap>>(HARD_CACHE_CAPACITY / 2);

    private final Handler purgeHandler = new Handler();

    private final Runnable purger = new Runnable() {
        public void run() {
//            clearCache();
        }
    };

    /**
     * Adds this bitmap to the cache.
     * @param bitmap The newly downloaded bitmap.
     */
    private void addBitmapToCache(String url, Bitmap bitmap) {
        if (bitmap != null) {
            synchronized (sHardBitmapCache) {
                sHardBitmapCache.put(url, bitmap);
            }
        }
    }

    /**
     * @param url The URL of the image that will be retrieved from the cache.
     * @return The cached bitmap or null if it was not found.
     */
    private Bitmap getBitmapFromCache(String url) {
        // First try the hard reference cache
        synchronized (sHardBitmapCache) {
            final Bitmap bitmap = sHardBitmapCache.get(url);
            if (bitmap != null) {
                // Bitmap found in hard cache
                // Move element to first position, so that it is removed last
                sHardBitmapCache.remove(url);
                sHardBitmapCache.put(url, bitmap);
                return bitmap;
            }
        }

        // Then try the soft reference cache
        if(url != null && sSoftBitmapCache.get(url) != null){
        	SoftReference<Bitmap> bitmapReference = sSoftBitmapCache.get(url);
	        if (bitmapReference != null) {
	            final Bitmap bitmap = bitmapReference.get();
	            if (bitmap != null) {
	                // Bitmap found in soft cache
	                return bitmap;
	            } else {
	                // Soft reference has been Garbage Collected
	                sSoftBitmapCache.remove(url);
	            }
	        }
        }
        return null;
    }
 
    /**
     * Clears the image cache used internally to improve performance. Note that for memory
     * efficiency reasons, the cache will automatically be cleared after a certain inactivity delay.
     */
//    public void clearCache() {
//        sHardBitmapCache.clear();
//        sSoftBitmapCache.clear();
//    }

    /**
     * Allow a new delay before the automatic cache clear is done.
     */
    private void resetPurgeTimer() {
        purgeHandler.removeCallbacks(purger);
        purgeHandler.postDelayed(purger, DELAY_BEFORE_PURGE);
    }
}

