package com.epon;

/**
 * Created by macbook on 28/12/14.
 */
public class Utils {
    public static String[] getSeparatedStrings(String separate, String text){
        String[] separated = text.split(separate);
        return separated;
    }

    public static  String getDeclension(int number){
        String text = Integer.toString(number);
        int size = text.length();
        if (size>1&&text.charAt(size-2)=='1'){
            return "";
        }else if (text.charAt(size-1)=='1') {
            return "о";
        }else if (text.charAt(size-1)=='2'||text.charAt(size-1)=='3'||text.charAt(size-1)=='4'){
            return "a";
        }else {
            return "";
        }
    }

}
