package com.epon.DataBase;

import java.io.Serializable;

public class BasketData implements Serializable{

    private int id;
    private String name;
    private String amount;
    private String price;
    private String summ;
    private int menuId;
    private String url;
    private String desc;
    private String parent;


    public BasketData(int id, String name, String amount, String price, String summ, int menuId, String url,String desc,String parent) {

        this.id = id;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.summ = summ;
        this.menuId = menuId;
        this.url = url;
        this.desc=desc;
        this.parent=parent;
    }


    public int getId() { return id; }
    public String getName () {
        return name;
    }
    public String getAmount () {
        return amount;
    }
    public String getPrice () {
        return price;
    }
    public String getSumm() { return  summ; }
    public int getMenuId() { return  menuId; }
    public String getUrl () {
        return url;
    }
    public String getDesc(){ return  desc;}
    public String getParent(){return  parent;}

}