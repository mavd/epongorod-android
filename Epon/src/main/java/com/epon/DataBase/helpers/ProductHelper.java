package com.epon.DataBase.helpers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.epon.DataBase.DBHelper;
import com.epon.DataBase.Product;

import java.util.ArrayList;

/**
 * Created by macbook on 03/01/15.
 */
public class ProductHelper {
    Context context;
    DBHelper helper;
    SQLiteDatabase db;

    public static final String TABLE_NAME="epon_product";
    public static final String TABLE_PRODUCT_PHOTO="epon_product_photo";

    public ProductHelper(Context context,DBHelper helper,SQLiteDatabase db){
        this.context=context;
        this.helper= helper;
        this.db = db;
    }

    public ArrayList<Product> getProductsByTag(int tag_id, int parent_id){
        Cursor cursor = null;
        String orderBy="\"order\"";
        ArrayList<Product> products= new ArrayList<Product>();
        String where ="id in (select product_id from "+ CategoryHelper.TABLE_PRODUCT_TAG+ " where tag_id = " + Integer.toString(tag_id)+" )" +
                " and category_id =  "+Integer.toString(parent_id) +"  and is_active = 1";
        cursor= db.query(TABLE_NAME,null,where,null,null,null,orderBy);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Product product= cursorToProduct(cursor);
            products.add(product);
            cursor.moveToNext();
        }
        return products;
    }

    public Product cursorToProduct(Cursor cursor){
        Product product= new Product();
        product.id = cursor.getInt(cursor.getColumnIndex("id"));
        product.category_id= cursor.getInt(cursor.getColumnIndex("category_id"));
        product.isActive=  cursor.getInt(cursor.getColumnIndex("is_active"));
        product.order =  cursor.getInt(cursor.getColumnIndex("order"));
        product.size =  cursor.getInt(cursor.getColumnIndex("size"));
        product.cost =  cursor.getInt(cursor.getColumnIndex("cost"));
        product.weight =  cursor.getInt(cursor.getColumnIndex("weight"));
        product.title= cursor.getString(cursor.getColumnIndex("title"));
        product.description =  cursor.getString(cursor.getColumnIndex("description"));
        product.photo = getPhotoProductById(product.id);
        return product;
    }

    public String getPhotoProductById(int id){
        Cursor cursor= null;
        String where = "product_id = " + Integer.toString(id);
        String[] photo_item = {"photo"};
        cursor = db.query(TABLE_PRODUCT_PHOTO,photo_item,where,null,null,null,null);
        cursor.moveToFirst();
        String photo= null;
        if (!cursor.isAfterLast()){
            photo= cursor.getString(0);
        }
        return photo;
    }

    public Product getProductById(int id){
        Cursor cursor= null;
        Product product= new Product();
        String where = "id = " + Integer.toString(id);
        cursor = db.query(TABLE_NAME,null,where,null,null,null,null);
        cursor.moveToFirst();
        String photo= null;
        if (!cursor.isAfterLast()){
            product = cursorToProduct(cursor);
        }
        return product;
    }


}
