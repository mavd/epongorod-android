package com.epon.DataBase.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.epon.DataBase.Category;
import com.epon.DataBase.DBHelper;
import com.epon.DataBase.Product;
import com.epon.EponApp;

import java.util.ArrayList;

/**
 * Created by macbook on 28/12/14.
 */
public class AllHelper  {
    DistrictHelper districtHelper;
    PlaceHelper placeHelper;
    CategoryHelper categoryHelper;
    TagHelper tagHelper;
    ProductHelper productHelper;
    ImageHelper imageHelper;
    Context mContext;
    DBHelper helper;
    SQLiteDatabase mDb;

    public AllHelper(Context context){
        try {
            mContext = context;
            helper = new DBHelper(context);
            mDb = helper.getReadableDatabase();
            districtHelper = new DistrictHelper(context, helper, mDb);
            placeHelper = new PlaceHelper(context, helper, mDb);
            categoryHelper = new CategoryHelper(context, helper, mDb);
            tagHelper= new TagHelper(context,helper,mDb);
            productHelper = new ProductHelper(context,helper,mDb);
            imageHelper = new ImageHelper(context,helper,mDb);
            EponApp.getInstance().id_epon = categoryHelper.getMainId("Епонский Городовой");
            EponApp.getInstance().id_uno= categoryHelper.getMainId("Уно Моменто");
            EponApp.getInstance().id_usuall=tagHelper.getTagIDByString("Обычное меню");
            EponApp.getInstance().id_spec=tagHelper.getTagIDByString("Спецпредложение");
            EponApp.getInstance().id_post =tagHelper.getTagIDByString("Постная еда");
            /*ArrayList<Category> categories= categoryHelper.getCategoriesForTag(id_post,id_epon);
            ArrayList<Product> products= productHelper.getProductsByTag(id_post,79);
            Product product = productHelper.getProductById(129);
            int u=1;
            u++;*/
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public DistrictHelper getDistrictHelper() {
        return districtHelper;
    }

    public PlaceHelper getPlaceHelper() {
        return placeHelper;
    }

    public ImageHelper getImageHelper() {
        return imageHelper;
    }

    public CategoryHelper getCategoryHelper() {
        return categoryHelper;
    }

    public ProductHelper getProductHelper() {
        return productHelper;
    }

    public TagHelper getTagHelper() {
        return tagHelper;
    }
}
