package com.epon.DataBase.helpers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.epon.DataBase.DBHelper;
import com.epon.network.ImageFromServer;

/**
 * Created by macbook on 03/01/15.
 */
public class ImageHelper {
    DBHelper helper;
    SQLiteDatabase db;
    Context context;
    public static final String TABLE_NAME="epon_images";
    public ImageHelper(Context context,DBHelper helper,SQLiteDatabase db){
        this.context= context;
        this.helper = helper;
        this.db = db;
    }

    public String getUrlByTitleAndCategory(String category,String title){
        Cursor cursor = null;
        String[] url_item={"url"};
        String img_url="";
        String where="title = \""+title+ "\" and type = \""+ category+"\"";
        cursor= db.query(TABLE_NAME,url_item,where,null,null,null,null);
        cursor.moveToFirst();
        if (!cursor.isAfterLast()){
            img_url=cursor.getString(0);
        }
        return img_url;
    }

}
