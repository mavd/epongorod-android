package com.epon.DataBase.helpers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.epon.DataBase.Category;
import com.epon.DataBase.DBHelper;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by macbook on 03/01/15.
 */
public class CategoryHelper  {
    public final static String TABLE_CATEGORY="epon_category";
    Context context;
    DBHelper helper;
    SQLiteDatabase db;
    public final static String TABLE_PRODUCT="epon_product";
    public final static String TABLE_PRODUCT_TAG="epon_product_to_tag";

    public CategoryHelper(Context context,DBHelper dbHelper,SQLiteDatabase db){
        this.context=context;
        this.helper=helper;
        this.db=db;
    }

    public int getMainId(String text){
        Cursor cursor=null;
        String[] id={"id"};
        String where="title = \""+text+"\"";
        cursor= db.query(TABLE_CATEGORY,id,where,null,null,null,null);
        cursor.moveToFirst();
        if  (!cursor.isAfterLast()) {
            return cursor.getInt(0);
        }else{
            return 0;
        }
    }

    public ArrayList<Category> getCategoriesForTag(int tag, int parent){
        ArrayList<Category> categories= new ArrayList<Category>();
        Cursor cursor=null;
        String orderBy="\"order\"";
        String where= "id in (select category_id from " +  TABLE_PRODUCT+"  where id in (select product_id from "+TABLE_PRODUCT_TAG+
                " where tag_id = "+Integer.toString(tag)+" )) and parent_category_id = "+Integer.toString(parent) +" and is_active = 1";
        cursor= db.query(TABLE_CATEGORY,null,where,null,null,null,orderBy);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Category category= cursorToCategory(cursor);
            cursor.moveToNext();
            categories.add(category);
        }
        return categories;
    }

    public Category cursorToCategory(Cursor cursor){
        Category category= new Category();
        category.id=cursor.getInt(cursor
                .getColumnIndex("id"));
        category.is_active =cursor.getInt(cursor
                .getColumnIndex("is_active"));
        category.order = cursor.getInt(cursor
                .getColumnIndex("order"));
        category.parent_category_id = cursor.getInt(cursor
                .getColumnIndex("parent_category_id"));
        category.photo = cursor.getString(cursor
                .getColumnIndex("photo"));
        category.title =cursor.getString(cursor
                .getColumnIndex("title"));
        return category;
    }
}
