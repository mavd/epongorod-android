package com.epon.DataBase.helpers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.epon.DataBase.DBHelper;
import com.epon.DataBase.District;

import java.util.ArrayList;

/**
 * Created by macbook on 28/12/14.
 */
public class DistrictHelper {
    DBHelper helper;
    Context mContext;
    SQLiteDatabase mDb;
    ArrayList<District> districts= new ArrayList<District>();

    public static final String TABLE_NAME="epon_district";
    public DistrictHelper(Context context,DBHelper helper,SQLiteDatabase db){
        mContext=context;
        this.helper= helper;
        mDb=db;
        districts = getDistricts();
    }

    public ArrayList<District> getDistricts(){
        districts = new ArrayList<District>();
        Cursor cursor=null;
        String orderBy="\"order\"";
        String where = "is_active = 1";
        cursor= mDb.query(TABLE_NAME,null,where,null,null,null,orderBy);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            districts.add(districtFromCursor(cursor));
            cursor.moveToNext();
        }

        return districts;
    }

    public ArrayList<String> titleFromDistricts(){
        ArrayList<String> titles= new ArrayList<String>();
        for (District district:districts){
            titles.add(district.title.replace("\\",""));
        }
        return  titles;
    }


    public District districtFromCursor(Cursor cursor){
        District district= new District();
        district.id=cursor.getInt(0);
        district.title=cursor.getString(cursor
                .getColumnIndex("title"));
        district.min_order_sum=cursor.getInt(cursor
                .getColumnIndex("min_order_sum"));
        district.order = cursor.getInt(cursor
                .getColumnIndex("order"));
        district.is_active = cursor.getInt(cursor
                .getColumnIndex("is_active"));
        return district;
    }

    public District getDistrictById(long id){
      if (id<districts.size()){
          return districts.get((int)id);
      } else {
          return null;
      }
    }
}
