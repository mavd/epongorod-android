package com.epon.DataBase.helpers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.epon.DataBase.DBHelper;
import com.epon.DataBase.Tag;

import java.util.ArrayList;

/**
 * Created by macbook on 03/01/15.
 */
public class TagHelper {
    SQLiteDatabase db;
    DBHelper helper;
    Context conext;
    public static final String TABLE_NAME="epon_tag";
    public TagHelper(Context context,DBHelper helper,SQLiteDatabase db){
        this.conext=context;
        this.helper=helper;
        this.db=db;
    }
    public int  getTagIDByString(String text){
        Cursor cursor=null;
        String[] id={"id"};
        String where="title = \""+text+"\"";
        cursor= db.query(TABLE_NAME,id,where,null,null,null,null);
        cursor.moveToFirst();
        if  (!cursor.isAfterLast()) {
            return cursor.getInt(0);

        }else{
            return 0;
        }
    }

    public ArrayList<Tag> getTagByParentId(int parent_id){
        ArrayList<Tag> tags= new ArrayList<Tag>();
        Cursor cursor=null;
        String orderBy="\"order\"";
        String where="id in (select tag_id  from "+CategoryHelper.TABLE_PRODUCT_TAG+" where product_id in ( select id from "+
                CategoryHelper.TABLE_PRODUCT+ " where category_id in ( select id from  " + CategoryHelper.TABLE_CATEGORY+
                " where parent_category_id = "+Integer.toString(parent_id)+ " ))) and is_active = 1";
        cursor= db.query(TABLE_NAME,null,where,null,null,null,orderBy);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Tag newTag=cursorToTag(cursor);
            tags.add(newTag);
            cursor.moveToNext();
        }
        return tags;
    }

    public Tag cursorToTag(Cursor cursor){
        Tag tag= new Tag();

        tag.id=cursor.getInt(cursor
                .getColumnIndex("id"));
        tag.title=cursor.getString(cursor
                .getColumnIndex("title"));
        tag.description=cursor.getString(cursor
                .getColumnIndex("description"));
        tag.order=cursor.getInt(cursor
                .getColumnIndex("order"));
        tag.isActive=cursor.getInt(cursor
                .getColumnIndex("is_active"));
        return tag;
    }

}
