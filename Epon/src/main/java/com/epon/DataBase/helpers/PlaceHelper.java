package com.epon.DataBase.helpers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.epon.DataBase.Adress;
import com.epon.DataBase.DBHelper;
import com.epon.DataBase.District;

import java.util.ArrayList;

/**
 * Created by macbook on 28/12/14.
 */
public class PlaceHelper {
    DBHelper helper;
    Context mContext;
    SQLiteDatabase mDb;
    ArrayList<Adress> places =new ArrayList<Adress>();

    public static final String TABLE_NAME="epon_address";
    public PlaceHelper(Context context,DBHelper helper,SQLiteDatabase db){
        mContext=context;
        this.helper= helper;
        mDb=db;
        places = getPlaces();
    }

    public ArrayList<Adress> getPlaces(){
        places = new ArrayList<Adress>();
        Cursor cursor=null;
        String where = "is_active = 1";
        String orderBy="\"order\"";
        cursor= mDb.query(TABLE_NAME,null,where,null,null,null,orderBy);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            places.add(districtFromCursor(cursor));
            cursor.moveToNext();
        }

        return places;
    }

    public ArrayList<String> titleFromDistricts(){
        ArrayList<String> titles= new ArrayList<String>();
        for (Adress district:places){
            titles.add(district.title.replace("\\\\","\\"));
        }
        return  titles;
    }


    public Adress districtFromCursor(Cursor cursor){
        Adress district= new Adress();
        district.id=cursor.getInt(0);
        district.title=cursor.getString(cursor
                .getColumnIndex("title"));
        district.email=cursor.getString(cursor
                .getColumnIndex("email"));
        district.order = cursor.getInt(cursor
                .getColumnIndex("order"));
        district.is_active = cursor.getInt(cursor
                .getColumnIndex("is_active"));
        return district;
    }
}
