package com.epon.DataBase;

/**
 * Created by macbook on 28/12/14.
 */
public class District {
    public int id;
    public String title;
    public int min_order_sum;
    public int order;
    public int is_active;

    public District(){

    }
    public District(int id, String title, int min_order_sum, int order, int is_active) {
        this.id = id;
        this.title = title;
        this.min_order_sum = min_order_sum;
        this.order = order;
        this.is_active = is_active;
    }
}
