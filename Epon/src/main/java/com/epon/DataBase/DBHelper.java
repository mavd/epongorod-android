package com.epon.DataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "eponDB.db";
    private static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "menuTable";
    public static final String TABLE_NAME_2 = "orderTable";
    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
            "_id" + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "type" + " TEXT, " +
            "category" + " TEXT, " +
            "subcategory" + " TEXT, " +
            "name" + " TEXT, " +
            "composition" + " TEXT, " +
            "weight" + " TEXT, " +
            "diameter" + " TEXT, " +
            "price" + " TEXT, " +
            "url" + " TEXT ); ";

    public static final String SQL_CREATE_PRICE = "CREATE TABLE " + TABLE_NAME_2 + " (" +
            "_id" + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "name" + " TEXT unique, " +
            "amount" + " TEXT, " +
            "price" + " TEXT, " +
            "menuId" + " TEXT, " +
            "url" + " TEXT, " +
            "desc" + " TEXT ,"+"subcategory" + " TEXT "+" ); ";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
            + TABLE_NAME;
    public static final String SQL_DELETE_PRICE = "DROP TABLE IF EXISTS "
            + TABLE_NAME_2;


    public static final String DROP_IMAGES = "DROP TABLE IF EXISTS epon_images;";
    public static final String CREATE_IMAGES = "CREATE TABLE `epon_images` (\n" +
            "  `id` int(11) NOT NULL ,\n" +
            "  `type` varchar(150) NOT NULL,\n" +
            "  `url` varchar(150) NOT NULL,\n" +
            "`title` varchar(150) NOT NULL," +
            "  PRIMARY KEY (`id`)\n" +
            ") ;";
    public String getInsertImageString(int id, String type, String url, String title){
        String text = "INSERT INTO epon_images VALUES ( \'"+Integer.toString(id)+"\' , \'"+type+"\' , \'"+
                url+"\' , \'"+ title+"\' ); ";
        return text;
    }
    public DBHelper(Context context) {
        // TODO Auto-generated constructor stub
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(SQL_CREATE_ENTRIES);
        db.execSQL(SQL_CREATE_PRICE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        // Удаляем предыдущую таблицу при апгрейде
        try {
            db.execSQL(SQL_DELETE_ENTRIES);
            db.execSQL(SQL_DELETE_PRICE);
            // Создаём новый экземпляр таблицы
            onCreate(db);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





}