package com.epon.DataBase;

import java.io.Serializable;

/**
 * Created by macbook on 28/12/14.
 */
public class Adress implements Serializable {
    public int id;
    public String title;
    public String email;
    public int order;
    public int is_active;

    public Adress(){}
    public Adress(String title, int id, String email, int order, int is_active) {
        this.title = title;
        this.id = id;
        this.email = email;
        this.order = order;
        this.is_active = is_active;
    }
}
