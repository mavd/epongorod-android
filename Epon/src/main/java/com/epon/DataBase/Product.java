package com.epon.DataBase;

import java.io.Serializable;

/**
 * Created by macbook on 28/12/14.
 */
public class Product implements Serializable {
    public int id;
    public int category_id;
    public String title;
    public String description;
    public int cost;
    public int weight;
    public int size;
    public int order;
    public int isActive;
    public String photo;


    public Product(int id, int category_id, String title, String description, int cost, int weught,
                   int size, int order, int isActive) {

        this.id = id;
        this.category_id = category_id;
        this.title = title;
        this.description = description;
        this.cost = cost;
        this.weight = weught;
        this.size = size;
        this.order = order;
        this.isActive = isActive;
    }

    public Product(){

    }
}
