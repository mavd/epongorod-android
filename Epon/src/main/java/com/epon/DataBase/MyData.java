package com.epon.DataBase;

import com.epon.R;

import java.io.Serializable;

public class MyData implements Serializable{

    private int id;
	private String type;
    private String category;
    private String subcategory;
    private String name;
    private String composition;
    private String weight;
    private String diameter;
    private String price;
    private String url;


	public MyData (int id, String type, String category, String subcategory, String name, String composition, String weight, String diameter,
                   String price, String url) {

        this.id = id;
		this.type = type;
		this.category = category;
		this.subcategory = subcategory;
        this.name = name;
        this.composition = composition;
        this.weight = weight;
        this.diameter = diameter;
        this.price = price;
        this.url = url;
	}


    public int getId() { return id; }
    public String getType () {
        return type;
    }
    public String getCategory () {
        return category;
    }
    public String getSubcategory () {
        return subcategory;
    }
    public String getName () {
        return name;
    }
    public String getComposition () {
        return composition;
    }
    public String getWeight () {
        return weight;
    }
    public String getDiameter () {
        return diameter;
    }
    public String getPrice () {
        return price;
    }
    public String getUrl () {
        return url;
    }

}