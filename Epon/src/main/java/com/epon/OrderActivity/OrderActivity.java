package com.epon.OrderActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.AppActiveService;
import com.epon.R;
import com.epon.lazyList.FileCache;

public class OrderActivity extends ActionBarActivity {

    private ActionBar mActionBar;
    ViewGroup actionBarLayout;

    //Intent
    String summPrice;

    //Fragmnts
    android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction;

    OrderFragmentPartOne orderFragmentPartOne;

    private boolean mAppActiveServiceBound = false;
    private AppActiveService mAppActiveService = null;
    private ServiceConnection mAppActiveConnection = new ServiceConnection() {
        public void onServiceConnected( ComponentName className, IBinder service ) {
            mAppActiveService = ( (AppActiveService.AppActiveBinder) service ).getService();
        }
        public void onServiceDisconnected( ComponentName className ) {
            mAppActiveService = null;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_order);

        //Intents
        summPrice = getIntent().getStringExtra("summPrice");

        mActionBar = getSupportActionBar();
        actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.actionbar_header_order, null);

        orderFragmentPartOne = new OrderFragmentPartOne();
        Bundle bundle = new Bundle();
        bundle.putString("price", summPrice);

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frgmContOrder, orderFragmentPartOne);
        orderFragmentPartOne.setArguments(bundle);
        fragmentTransaction.commit();



        setActionBarTitle1(summPrice);
    }

    public void setActionBarTitle1 (String title) {

        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setCustomView(actionBarLayout);

        TextView bas = (TextView) findViewById(R.id.priceOrder);
        bas.setText(title);
    }

    public void onDestroy() {
        super.onDestroy();
        /*FileCache fileCache = new FileCache(this);
        fileCache.clear();*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAppActiveServiceBound = bindService( new Intent( this, AppActiveService.class ), mAppActiveConnection, Context.BIND_AUTO_CREATE );
    }

    @Override
    protected void onStop() {
        super.onStop();
        if( mAppActiveServiceBound ) {
            unbindService( mAppActiveConnection );
            mAppActiveServiceBound = false;
        }
    }

}