package com.epon.OrderActivity;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.epon.ApiFacade;
import com.epon.DataBase.DBHelper;
import com.epon.EponApp;
import com.epon.MainActivity.MainActivity;
import com.epon.R;
import com.epon.network.*;
import com.views.LinedEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class OrderFragmentPartTwo extends Fragment {

    //DB
    private DBHelper dbHelper;
    private SQLiteDatabase mDataBase;

    //Intents
    private String messageText;
    private String city;
    private String name;
    private String telNumber;
    private String address;
    private String dist;
    private String place;
    private String deliver;

    //Time
    private TimePicker timePicker;
    private Calendar calendar_date = null;
    private DatePicker datePicker;

    private EditText amountEditText;
    private Button minusBtn;
    private Button plusBtn;

    //Layout
    LinearLayout dateTimeLin;
    TextView textViewDateTime;

    private EditText editTextAdditional;

    private CheckBox chkBox;

    private Button makeAnOrder;

    ImageButton button;


    boolean firstTime = true;
    TextView txtComment;
    private ProgressBar progressBar;
    LinearLayout layoutCheck;
    View lineCheck;
    int payType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_order_2part,
                container, false);

        amountEditText = (EditText) rootView.findViewById(R.id.amountEditTextOrder);
        minusBtn = (Button) rootView.findViewById(R.id.bntMinus);
        plusBtn = (Button) rootView.findViewById(R.id.bntPlus);
        makeAnOrder = (Button) rootView.findViewById(R.id.makeAnorder);

        editTextAdditional = (EditText) rootView.findViewById(R.id.editTextAdditional);
        chkBox = (CheckBox) rootView.findViewById(R.id.chkBox);
        button = (ImageButton) rootView.findViewById(R.id.timeBtn);

        dateTimeLin = (LinearLayout) rootView.findViewById(R.id.DateTimeLayout);
        textViewDateTime = (TextView) rootView.findViewById(R.id.textViewDateTime);
        txtComment = (TextView) rootView.findViewById(R.id.txtComment);
        txtComment.setKeyListener(null);
        layoutCheck=(LinearLayout)rootView.findViewById(R.id.layoutCheck);
        lineCheck=rootView.findViewById(R.id.lineUnderCheck);

        /*String text = getString(R.string.additional);
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
        txtComment.setText(content);*/
        //GetIntent
        city = getArguments().getString("city");
        name = getArguments().getString("name");
        telNumber = getArguments().getString("telNumber");
        address = getArguments().getString("address");
        dist = getArguments().getString("dist");
        deliver = getArguments().getString("deliver");
        place =getArguments().getString("place");
        payType=getArguments().getInt("payType");
        if (place!=null) {
            if (place.toUpperCase().indexOf(getString(R.string.yourself)) == 0) {
                layoutCheck.setVisibility(View.GONE);
                lineCheck.setVisibility(View.GONE);
            }
        }
        //Number picker
        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getTextAmount = amountEditText.getText().toString();
                int intAmount = Integer.parseInt(getTextAmount);

                if (intAmount > 1) {
                    --intAmount;
                }
                getTextAmount = String.valueOf(intAmount);
                amountEditText.setText(getTextAmount);
            }
        });

        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getTextAmount = amountEditText.getText().toString();
                int intAmount = Integer.parseInt(getTextAmount);

                if (intAmount < 15) {
                    ++intAmount;
                }
                getTextAmount = String.valueOf(intAmount);
                amountEditText.setText(getTextAmount);
            }
        });

        //DateTime
        button.setEnabled(false);

        chkBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {

                    //timePicker.setVisibility(View.VISIBLE);
                    dateTimeLin.setVisibility(View.VISIBLE);
                    button.setEnabled(true);

                } else {

                    //datePicker.setVisibility(View.INVISIBLE);
                    //timePicker.setVisibility(View.INVISIBLE);
                    button.setEnabled(false);
                    dateTimeLin.setVisibility(View.GONE);
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*if (timePicker.getVisibility()== View.VISIBLE) {
                    datePicker.setVisibility(View.VISIBLE);
                    timePicker.setVisibility(View.INVISIBLE);

                } else {
                    datePicker.setVisibility(View.INVISIBLE);
                    timePicker.setVisibility(View.VISIBLE);
                }*/

                show();
            }
        });
        chkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    dateTimeLin.setVisibility(View.VISIBLE);
                    button.setEnabled(true);
                    show();
                }else {
                    button.setEnabled(false);
                    dateTimeLin.setVisibility(View.GONE);
                }
            }
        });
        dateTimeLin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show();
            }
        });

        makeAnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String timeDateString = "";
                timeDateString = textViewDateTime.getText().toString();
                JSONObject o = new JSONObject();
                try {
                    o.accumulate("adress",address);
                    o.accumulate("timeDate",timeDateString);
                    o.accumulate("name",name);
                    o.accumulate("amountPerson",amountEditText.getText().toString());
                    o.accumulate("comment",editTextAdditional.getText().toString());
                    o.accumulate("dist",place);
                    o.accumulate("telNumber","+7"+telNumber);
                    o.accumulate("delivery","");
                    o.accumulate("city",dist);
                    o.accumulate("paymentType",payType+1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                messageText = "Город: " + city + "\n" +
                        "Имя: " + name + "\n" +
                        "Телефон: " + telNumber + "\n" +
                        "Район: " + dist + "\n" +
                        "Адрес: " + address + "\n" +
                        "Кол-во персон: " + amountEditText.getText().toString() + "\n" +
                        "Способ доставки: " + deliver + "\n" +
                        "Коммент.: " + editTextAdditional.getText().toString() + "\n" +
                        "Время: " + timeDateString + "\n" +
                        "------------------------------------" + "\n" +
                        "Выбранные блюда: " + "\n";

                dbHelper = new DBHelper(getActivity());
                mDataBase = dbHelper.getReadableDatabase();
                String goods="";
                Cursor mCursor = null;
                if (mDataBase != null) {
                    mCursor = mDataBase.query("orderTable", null, null, null, null, null, null);
                    mCursor.moveToFirst();
                    if (!mCursor.isAfterLast()) {
                        do {

                            String name = mCursor.getString(mCursor
                                    .getColumnIndex("name"));
                            String amount = mCursor.getString(mCursor
                                    .getColumnIndex("amount"));
                            String parent=mCursor.getString(mCursor
                                    .getColumnIndex("subcategory"));
                            String cost= mCursor.getString(mCursor.getColumnIndex("price"));
                            messageText +=" "+ parent +" \"" + name + "\" - " + amount + "\n";
                            goods+=" "+parent +" \"" + name + "\", " + amount + ", "+cost+" р."+"\n";
                        } while (mCursor.moveToNext());
                    }

                    mCursor.close();
                    mDataBase.close();
                }
                try {
                    o.accumulate("goods",goods);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setProgress(true);
                ApiFacade.getInstance().sentOrder(o.toString(),new Listener() {
                    @Override
                    public void onResponse(BaseRequest request) {
                        /*EponApp.getInstance().clearPrice();
                        Intent intent = new Intent(getActivity(), FinalActivity.class);
                        startActivity(intent);*/
                        EponApp.getInstance().showToast(getString(R.string.order_accept));
                        EponApp.getInstance().clearPrice();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                        setProgress(false);
                    }
                    @Override
                    public void onError(com.epon.network.Error error, BaseRequest request) {
                        if (error==null||error.code==-1){
                            EponApp.getInstance().showToast(getString(R.string.check_internet));
                        }
                        setProgress(false);
                    }
                });
                /*Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{"eponasdfsadgdag@gmail.com"});
                email.putExtra(Intent.EXTRA_SUBJECT, "Отправлено с мобильного приложения");
                email.putExtra(Intent.EXTRA_TEXT, messageText);
                email.setType("message/rfc822");
                startActivity(email);*/

            }
        });

        txtComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setComment();
            }
        });
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        return rootView;
    }

    public void setComment() {
        if (editTextAdditional.getVisibility() == View.VISIBLE) {
            editTextAdditional.setVisibility(View.GONE);
        } else {
            editTextAdditional.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        /*if (firstTime) {

            firstTime = false;
        } else {

            Intent intent = new Intent(getActivity(), FinalActivity.class);
            startActivity(intent);
            firstTime = true;

        }*/
    }


    //DateTime Dialog
    public void show() {

        final Dialog d = new Dialog(getActivity());
        d.setTitle("Выбор даты и времени ");
        d.setContentView(R.layout.dialog_date_time);
        d.setCancelable(false);
        timePicker = (TimePicker) d.findViewById(R.id.timePickerOrder);
        timePicker.setIs24HourView(true);
        datePicker = (DatePicker) d.findViewById(R.id.datePicker);
        datePicker.setMinDate(System.currentTimeMillis()-1000);
        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);
        ImageButton dateTime = (ImageButton) d.findViewById(R.id.timeBtnDialog);

        if (calendar_date == null)
            calendar_date = Calendar.getInstance();
        timePicker.setCurrentHour(calendar_date.get(Calendar.HOUR_OF_DAY)+1);
        timePicker.setCurrentMinute(calendar_date.get(Calendar.MINUTE));

        dateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (timePicker.getVisibility() == View.VISIBLE) {
                    datePicker.setVisibility(View.VISIBLE);
                    timePicker.setVisibility(View.INVISIBLE);

                } else {
                    datePicker.setVisibility(View.INVISIBLE);
                    timePicker.setVisibility(View.VISIBLE);
                }
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar= Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                int hour=calendar.get(Calendar.HOUR_OF_DAY);
                if (hour>=timePicker.getCurrentHour()){
                    EponApp.getInstance().showToast(getString(R.string.enter_time));
                    chkBox.setChecked(false);
                }else {
                    String curTime = String.format("%02d:%02d", timePicker.getCurrentHour(), timePicker.getCurrentMinute());
                    String strDateTime = datePicker.getDayOfMonth() + "-" + (datePicker.getMonth() + 1) + "-" +
                            datePicker.getYear() + " " + curTime;
                    textViewDateTime.setText(strDateTime);
                }
                d.dismiss();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chkBox.setChecked(false);
                d.dismiss(); // dismiss the dialog
            }
        });

        d.show();
    }

    public void setProgress(boolean visible) {
        if (visible) {
            progressBar.setVisibility(View.VISIBLE);
            makeAnOrder.setVisibility(View.GONE);
        }else {
            progressBar.setVisibility(View.GONE);
            makeAnOrder.setVisibility(View.VISIBLE);
        }
    }
}