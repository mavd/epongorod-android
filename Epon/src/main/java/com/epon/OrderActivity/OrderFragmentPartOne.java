package com.epon.OrderActivity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.TimeUtils;
import com.Util;
import com.epon.DataBase.District;
import com.epon.DataBase.helpers.DistrictHelper;
import com.epon.DataBase.helpers.PlaceHelper;
import com.epon.EponApp;
import com.epon.R;
import com.epon.adapters.MySpinnerAdapter;
import com.views.CustomEditText;

import java.util.Arrays;

public class OrderFragmentPartOne extends Fragment {

    //Intent
    String price;


    //SharedPreferences
    SharedPreferences sPref;

    //Radio
    private RadioGroup radioCity;
    private RadioButton radioCityButton;
    private RadioGroup radioDelev;
    private RadioButton radioDelevButton;
    private Spinner spinerDist, spinerPlace,spinerPayType;

    //ID's of elements
    private EditText editTextName;
    private EditText editPhone;
    private EditText editTextAddress;

    private Button nextFragment;
    DistrictHelper distHelper;
    PlaceHelper placeHelper;
    boolean format;
    LinearLayout layoutDistrict;
    FrameLayout  layoutAdress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_order,
                container, false);
        distHelper = EponApp.getInstance().getAllHelper().getDistrictHelper();
        placeHelper = EponApp.getInstance().getAllHelper().getPlaceHelper();
        editTextName = (EditText) rootView.findViewById(R.id.editTextName);
        editPhone = (EditText) rootView.findViewById(R.id.editTextTelephone);
        editPhone.addTextChangedListener(new NumberChangedListener());
        editTextAddress = (EditText) rootView.findViewById(R.id.editTextAddress);
        spinerDist = (Spinner) rootView.findViewById(R.id.spinnertDist);
        spinerPlace = (Spinner) rootView.findViewById(R.id.spinnertPlace);
        spinerPayType=(Spinner)rootView.findViewById(R.id.spinnerPaymentType);
        radioCity = (RadioGroup) rootView.findViewById(R.id.radioCity);
        radioDelev = (RadioGroup) rootView.findViewById(R.id.radioDeliver);
        nextFragment = (Button) rootView.findViewById(R.id.makeNextButton);
        layoutAdress=(FrameLayout)rootView.findViewById(R.id.lin3);
        layoutDistrict=(LinearLayout)rootView.findViewById(R.id.layoutDistrict);
        //((OrderActivity) getActivity()).setActionBarTitle1("Your title");
        //Get Intent
        price = getArguments().getString("price");

        //Preferences
        loadPreferences();

       /* ArrayAdapter<String> adapter= new ArrayAdapter<String>(getActivity(),
                distHelper.getDistricts(),
                R.layout.spinner_row);*/
        MySpinnerAdapter distAdapter = new MySpinnerAdapter(getActivity(), R.layout.spinner_row, distHelper.titleFromDistricts());
        distAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        MySpinnerAdapter placeAdapter = new MySpinnerAdapter(getActivity(), R.layout.spinner_row, placeHelper.titleFromDistricts());
        placeAdapter.setDropDownViewResource(R.layout.spiner_dropdown_item);
        MySpinnerAdapter payAdapter=new MySpinnerAdapter(getActivity(), R.layout.spinner_row, Arrays.<String>asList(getResources().getStringArray(R.array.payment_type)));
        payAdapter.setDropDownViewResource(R.layout.spiner_dropdown_item);
        spinerPayType.setAdapter(payAdapter);
        spinerDist.setAdapter(distAdapter);
        spinerDist.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                /*InputMethodManager imm = (InputMethodManager) getActivity()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);

                if (imm.isAcceptingText()) {
                    hideSoftKeyboard(getActivity());
                }*/

                spinerDist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                                         @Override
                                                         public void onItemSelected(AdapterView<?> parent, View view, int pos,
                                                                                    long id) {
                                                             ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                                                             ((TextView) parent.getChildAt(0)).setTextSize(14);
                                                         }

                                                         @Override
                                                         public void onNothingSelected(AdapterView<?> arg0) {
                                                             hideSoftKeyboard(getActivity());
                                                         }
                                                     }
                );
                return false;
            }
        });
        spinerPlace.setAdapter(placeAdapter);
        nextFragment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                savePreferences();

                //City radio
                int selectedId = radioCity.getCheckedRadioButtonId();
                radioCityButton = (RadioButton) rootView.findViewById(selectedId);

                //Delev radio
                int selectedId2 = radioDelev.getCheckedRadioButtonId();
                radioDelevButton = (RadioButton) rootView.findViewById(selectedId2);

                String priceSplit[] = price.split("р");
                price = priceSplit[0];
                Double doublePrice = Double.valueOf(price);
                String phone = editPhone.getText().toString().replaceAll("[^\\d.]", "");
                if (editTextName.getText().toString().equalsIgnoreCase("")
                        || editPhone.getText().toString().equalsIgnoreCase("")
                        || spinerDist.getSelectedItem().toString().equalsIgnoreCase("Выберите район")
                        || (editTextAddress.getText().toString().equalsIgnoreCase("")&&layoutAdress.getVisibility()==View.VISIBLE)) {
                    Toast toast = Toast.makeText(getActivity(), "Введите обязательные поля", Toast.LENGTH_LONG);
                    toast.show();

                } else if (phone.length() > 0 && phone.length() < 10) {
                    Toast toast = Toast.makeText(getActivity(), "Проверьте количество цифр в номере телефона", Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    long district_id = spinerDist.getSelectedItemId();
                    District district = distHelper.getDistrictById(district_id);
                    if (district != null) {
                        if (doublePrice < district.min_order_sum) {
                            Toast toast = Toast.makeText(getActivity(), "Сумма Вашего заказа ниже необходимого минимума для выбранного района."
                                    + "\n" + "Пожалуйста, пополните заказ на сумму больше " + district.min_order_sum + " р.", Toast.LENGTH_LONG);
                            toast.show();
                        } else {
                            goToNextFragment();
                        }
                    }
                    /*String radioString = radioDelevButton.getText().toString();
                    if (radioString.equalsIgnoreCase("Курьером")) {

                        if (spinerDist.getSelectedItem().toString().equalsIgnoreCase("мкр. Текстильщик")
                                || spinerDist.getSelectedItem().toString().equalsIgnoreCase("мкр. Первомайский")
                                || spinerDist.getSelectedItem().toString().equalsIgnoreCase("р-н Завокзальный")
                                || spinerDist.getSelectedItem().toString().equalsIgnoreCase("Лесные поляны")) {

                            if (doublePrice < 600) {

                                Toast toast = Toast.makeText(getActivity(), "Сумма Вашего заказа ниже необходимого минимума для выбранного района."
                                        + "\n" + "Пожалуйста, пополните заказ на сумму больше 600р.",Toast.LENGTH_LONG);
                                toast.show();

                            } else {
                                goToNextFragment();
                            }

                        } else if (spinerDist.getSelectedItem().toString().equalsIgnoreCase("р-н Горки")
                                || spinerDist.getSelectedItem().toString().equalsIgnoreCase("Валентиновка")) {

                            if (doublePrice < 800) {

                                Toast toast = Toast.makeText(getActivity(), "Сумма Вашего заказа ниже необходимого минимума для выбранного района."
                                        + "\n" + "Пожалуйста, пополните заказ на сумму больше 800р.",Toast.LENGTH_LONG);
                                toast.show();

                            } else {
                                goToNextFragment();
                            }

                        } else if (spinerDist.getSelectedItem().toString().equalsIgnoreCase("р-н Загорянка")
                                || spinerDist.getSelectedItem().toString().equalsIgnoreCase("Ярославский пр-д")) {

                            if (doublePrice < 1000) {

                                Toast toast = Toast.makeText(getActivity(), "Сумма Вашего заказа ниже необходимого минимума для выбранного района."
                                        + "\n" + "Пожалуйста, пополните заказ на сумму больше 1000р.",Toast.LENGTH_SHORT);
                                toast.show();

                            } else {
                                goToNextFragment();
                            }
                        }

                    } else {
                        goToNextFragment();
                    }*/
                }
            }
        });
        spinerPlace.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setAdressVisibility();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return rootView;
    }

    public void setAdressVisibility(){
        String place = spinerPlace.getSelectedItem().toString();
        if (place!=null){
            if (place.toUpperCase().indexOf(getString(R.string.yourself))==0){
                layoutDistrict.setVisibility(View.GONE);
                layoutAdress.setVisibility(View.GONE);
            }else {
                layoutAdress.setVisibility(View.VISIBLE);
                layoutDistrict.setVisibility(View.VISIBLE);
            }
        }
    }

    private class NumberChangedListener implements TextWatcher {

        @Override
        public void afterTextChanged(Editable s) {
            String formattedNumber = "";
            if (Util.isEmpty(s))
                return;
            String formatS = s.toString().replaceAll("[^\\d.]", "");
            if (formatS.length() > 10) {
                formattedNumber = editPhone.getText().toString().substring(0, editPhone.getText().toString().length() - 1);
            } else {
                formattedNumber = TimeUtils.FormatStringAsPhoneNumber(formatS);
            }
            if (formattedNumber != null) {
                if (!s.toString().equals(formattedNumber) && !format) {
                    format = true;
                    editPhone.setText(formattedNumber);
                    editPhone.setSelection(editPhone.getText().length());
                    format = false;
                }

            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
    }

    private void savePreferences() {

        sPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("name", editTextName.getText().toString());
        ed.putString("telephone", editPhone.getText().toString());
        ed.putString("address", editTextAddress.getText().toString());
        ed.commit();
    }

    private void loadPreferences() {

        sPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String namePref = sPref.getString("name", "");
        String telephonePref = sPref.getString("telephone", "");
        String addressPref = sPref.getString("address", "");
        editTextName.setText(namePref);
        editPhone.setText(telephonePref);
        editTextAddress.setText(addressPref);
    }

    public void goToNextFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("city", radioCityButton.getText().toString());
        bundle.putString("name", editTextName.getText().toString());
        bundle.putString("telNumber", editPhone.getText().toString());
        bundle.putString("address", editTextAddress.getText().toString());
        bundle.putString("dist", Integer.toString(distHelper.getDistricts().get(spinerDist.getSelectedItemPosition()).id));
        bundle.putString("deliver", radioDelevButton.getText().toString());
        bundle.putString("place",Integer.toString(placeHelper.getPlaces().get(spinerPlace.getSelectedItemPosition()).id));
        bundle.putInt("payType",spinerPayType.getSelectedItemPosition());
        OrderFragmentPartTwo orderFragmentPartTwo = new OrderFragmentPartTwo();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frgmContOrder, orderFragmentPartTwo, "OrderFragmentPartTwo").addToBackStack(null);
        orderFragmentPartTwo.setArguments(bundle);
        ft.commit();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        savePreferences();
    }

}