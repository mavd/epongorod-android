package com.epon.fragments;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.epon.DataBase.Category;
import com.epon.DataBase.DBHelper;
import com.epon.DataBase.MyData;
import com.epon.DataBase.helpers.CategoryHelper;
import com.epon.EponApp;
import com.epon.R;
import com.epon.adapters.CategoryAdapter;
import com.epon.adapters.DBAdapter;
import com.epon.adapters.GridAdapter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


public class ListFragmentSushiLanten extends Fragment {

    //Rotation
    View rootView;

    private String currentPosition;

    private ListView listView;
    private GridView gridView;
    private GridAdapter gridAdapter;

    private FillTheList fillTheList;
    ViewPager mPager;

    //DB
    private DBAdapter dbAdaper;
    private DBHelper dbHelper;
    private SQLiteDatabase mDataBase;
    private CategoryHelper categoryHelper;
    private  String idSting;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (getRotation(getActivity()) == "landscape") {

            rootView = inflater.inflate(R.layout.fragment_grid_sushi,
                    container, false);
            gridView = (GridView) rootView.findViewById(R.id.gridView1);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //@Override
                public void onItemClick(AdapterView arg0, View v,
                                        int position, long id) {
                    idSting = ((TextView) v.findViewById(R.id.idOfSushiMenu)).getText().toString(); //Get id
                    currentPosition = ((TextView) v.findViewById(R.id.nameOfCafe)).getText().toString(); //Get name
                    goTonextFragment();
                }
            });

        } else {

            rootView = inflater.inflate(R.layout.fragment_list_sushi,
                    container, false);
            listView = (ListView) rootView.findViewById(android.R.id.list);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //@Override
                public void onItemClick(AdapterView arg0, View v,
                                        int position, long id) {
                    mPager.setVisibility(View.GONE);
                    idSting = ((TextView) v.findViewById(R.id.idOfSushiMenu)).getText().toString(); //Get id
                    currentPosition = ((TextView) v.findViewById(R.id.nameOfCafe)).getText().toString(); //Get name
                    goTonextFragment();
                }
            });
        }

        mPager = (ViewPager) getActivity().findViewById(R.id.pager);

        //DB
        dbHelper = new DBHelper(getActivity());
        categoryHelper= EponApp.getInstance().getAllHelper().getCategoryHelper();

        fillTheList = new FillTheList();
        fillTheList.execute();

        return rootView;
    }

    private void goTonextFragment() {

        mPager.setVisibility(View.GONE);
        Bundle bundle = new Bundle();
        bundle.putString("name", currentPosition);
        bundle.putString("category", "Постное Меню");
        bundle.putString("type", "Епонский Городовой");
        bundle.putInt("category_id",Integer.parseInt(idSting));
        bundle.putInt("tag_id",EponApp.getInstance().id_post);
        SubCategory frag2 = new SubCategory();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        frag2.setArguments(bundle);
        ft.replace(R.id.frgmCont, frag2, "SubCategoryFragment");
        ft.addToBackStack(null);
        ft.commit();
    }

    class FillTheList extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            ArrayList<Category> categories= categoryHelper.getCategoriesForTag(EponApp.getInstance().id_post,EponApp.getInstance().id_epon);
            if (getRotation(getActivity()) == "landscape") {
                /*gridAdapter = new GridAdapter(getActivity(), selectAll());
                gridView.setAdapter(gridAdapter);*/
                CategoryAdapter categoryAdapter= new CategoryAdapter(getActivity(),categories,EponApp.LANDSCAPE,0);
                gridView.setAdapter(categoryAdapter);
            } else {
                /*dbAdaper = new DBAdapter(getActivity(), selectAll());
                listView.setAdapter(dbAdaper);
                dbAdaper.notifyDataSetChanged();*/
                CategoryAdapter categoryAdapter= new CategoryAdapter(getActivity(),categories,EponApp.PORTRAIT,0);
                listView.setAdapter(categoryAdapter);
            }
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    } //End of FillTheList


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onPause() {
        super.onPause();
        fillTheList.cancel(false);
    }

    public ArrayList<MyData> selectAll() {

        Cursor mCursor = null;
        mDataBase = dbHelper.getReadableDatabase();
        mCursor = mDataBase.query("menuTable", null, null, null, null, null, null);

        ArrayList<MyData> arr = new ArrayList<MyData>();
        List<String> list1 = new ArrayList<String>();
        ArrayList<String> sortList = new ArrayList<String>();
        mCursor.moveToFirst();
        if (!mCursor.isAfterLast()) {
            do {

                String type = mCursor.getString(mCursor
                        .getColumnIndex("type"));
                String category = mCursor.getString(mCursor
                        .getColumnIndex("category"));
                if (type.equalsIgnoreCase("Епонский Городовой") && category.equalsIgnoreCase("Постное Меню")) {
                    String name = mCursor.getString(mCursor
                            .getColumnIndex("subcategory"));
                    list1.add(name);
                }

            } while (mCursor.moveToNext());
        }

        List<String> list2 = new ArrayList<String>();
        HashSet<String> lookup = new HashSet<String>();
        for (String item : list1) {
            if (lookup.add(item)) {
                // Set.add returns false if item is already in the set
                list2.add(item);
            }
        }
        list1 = list2;

        for (String value : list1) {

            mCursor.moveToFirst();
            if (!mCursor.isAfterLast()) {
                do {

                    String type = mCursor.getString(mCursor
                            .getColumnIndex("subcategory"));

                    if (type.equalsIgnoreCase(value)) {
                        String url = mCursor.getString(mCursor
                                .getColumnIndex("url"));
                        int id = mCursor.getInt(mCursor.getColumnIndex("_id"));

                        if (!sortList.contains(type)) {
                            sortList.add(type);
                            arr.add(new MyData(id, null, null, value, null, null, null, null, null, url));
                        }
                    }

                } while (mCursor.moveToNext());
            }
        }

        return arr;
    }//End of street magic

    public String getRotation(Context context){
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return "portrait";
            case Surface.ROTATION_90:
                return "landscape";
            case Surface.ROTATION_180:
                return "reverse portrait";
            default:
                return "reverse landscape";
        }
    }
}