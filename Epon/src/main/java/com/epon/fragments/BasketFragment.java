package com.epon.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsoluteLayout;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.epon.DataBase.BasketData;
import com.epon.DataBase.DBHelper;
import com.epon.MainActivity.MainActivity;
import com.epon.OrderActivity.OrderActivity;
import com.epon.R;
import com.epon.adapters.DBBasketAdapter;

import java.util.ArrayList;

public class BasketFragment extends ListFragment {

    ListView listView;

    private TextView summ;

    private double dblSumm;

    private String summString;
    private String idSting;
    private String idMenuSting;
    private String idName;
    private String amount;

    String idDelete;

    //Dialog
    EditText amountEditTextDialog;

    //DB
    private DBBasketAdapter dbAdaper;
    private DBHelper dbHelper;
    private SQLiteDatabase mDataBase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Deleting of a row
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {

                idDelete = ((TextView) arg1.findViewById(R.id.idOfSushiMenu)).getText().toString();

                showAlertDialog(getActivity(), "",
                        "Удалить выбранный элемент?", false);

                return true;
            }
        });

    }

    //List Item Click
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        idDelete = ((TextView) v.findViewById(R.id.idOfSushiMenu)).getText().toString();
        idSting = ((TextView) v.findViewById(R.id.idOfSushiMenu)).getText().toString(); //Get id of order position
        idMenuSting = ((TextView) v.findViewById(R.id.idOfMenu)).getText().toString(); //Get id of order position
        idName = ((TextView) v.findViewById(R.id.nameOfCafe)).getText().toString(); //Get name
        amount = ((TextView) v.findViewById(R.id.amount)).getText().toString();

        show();

        /*String idSting = ((TextView) v.findViewById(R.id.idOfSushiMenu)).getText().toString(); //Get id of order position
        String idMenuSting = ((TextView) v.findViewById(R.id.idOfMenu)).getText().toString(); //Get id of order position
        String idName = ((TextView) v.findViewById(R.id.nameOfCafe)).getText().toString(); //Get name
        Bundle bundle = new Bundle();
        bundle.putString("idOrder", idSting);
        bundle.putString("nameBasket", idName);
        bundle.putString("idMenu", idMenuSting);
        bundle.putInt("key", 1);

        SummaryItemFragment summaryItemFragment = new SummaryItemFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        summaryItemFragment.setArguments(bundle);

        String rotation = getRotation(getActivity());

        if (rotation.equalsIgnoreCase("portrait") || rotation.equalsIgnoreCase("reverse portrait")) {
            ft.replace(R.id.frgmCont, summaryItemFragment, "SummaryItemFragmentBasket").addToBackStack(null);

        } else {

            //ft.replace(R.id.cont, summaryItemFragment, "SummaryItemFragmentBasket").addToBackStack(null);
        }

        ft.commit();*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_basket, container, false);

        //Getting of Element's id
        if (rootView != null) {

            summ = (TextView) rootView.findViewById(R.id.priceBasket);
            listView = (ListView) rootView.findViewById(android.R.id.list);
        }

        //DB
        dbHelper = new DBHelper(getActivity());
        ArrayList<BasketData> data =selectAll();
        dbAdaper = new DBBasketAdapter(getActivity(), data);
        listView.setAdapter(dbAdaper);
        /*int height=0;
        for (int i=0;i<dbAdaper.getCount();i++){
            View childView = dbAdaper.getView(i, null, listView);
            childView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            height+=childView.getHeight();
        }
        listView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,height));*/
        Button button = (Button) rootView.findViewById(R.id.goToOrder);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent myIntent = new Intent(getActivity().getBaseContext(), OrderActivity.class);
                myIntent.putExtra("summPrice", summ.getText().toString());
                getActivity().startActivity(myIntent);
            }
        });

        return rootView;
    }

    public ArrayList<BasketData> selectAll() {

        Cursor mCursor = null;
        dbHelper = new DBHelper(getActivity());
        mDataBase = dbHelper.getReadableDatabase();
        mCursor = mDataBase.query("orderTable", null, null, null, null, null, null);

        dblSumm = 0;
        double dblprice;

        ArrayList<BasketData> arr = new ArrayList<BasketData>();

            mCursor.moveToFirst();
            if (!mCursor.isAfterLast()) {
                do {

                    int idCurs = mCursor.getInt(mCursor.getColumnIndex("_id"));

                    String name = mCursor.getString(mCursor
                            .getColumnIndex("name"));
                    String amount = mCursor.getString(mCursor
                            .getColumnIndex("amount"));
                    String price = mCursor.getString(mCursor
                            .getColumnIndex("price"));

                    int menuId = mCursor.getInt(mCursor
                            .getColumnIndex("menuId"));

                    String url = mCursor.getString(mCursor
                            .getColumnIndex("url"));
                    String desc= mCursor.getString(mCursor
                            .getColumnIndex("desc"));
                    String parent=mCursor.getString(mCursor
                            .getColumnIndex("subcategory"));
                    try {
                            dblprice = Double.valueOf(price);

                    } catch (NumberFormatException nfe) {
                        dblprice = 1;
                    }

                    double amountInt = Double.valueOf(amount);
                    amountInt = dblprice*amountInt;

                    String amountString = String.valueOf(amountInt);

                    dblSumm +=amountInt;
                    arr.add(new BasketData(idCurs, name, amount, price, amountString, menuId, url,desc,parent));
                } while (mCursor.moveToNext());
            }

        //Set Summ

        summString = String.valueOf(dblSumm);
        summ.setText(summString + "р.");
        if (getActivity()!=null) {
            ((MainActivity) getActivity()).setCostToActionBar((int) dblSumm);
            ((MainActivity) getActivity()).setmActionBarTitle(getString(R.string.basket).toUpperCase());
        }
        mCursor.close();
        mDataBase.close();
        return arr;

    }

    public void refreshList() {

        dbAdaper.setArrayMyData(selectAll());
        dbAdaper.notifyDataSetChanged();
    }


    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        //AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setNegativeButton("Отмена",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        alertDialog.setPositiveButton("Удалить",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

                dbHelper = new DBHelper(getActivity());
                mDataBase = dbHelper.getWritableDatabase();
                mDataBase.delete("orderTable", "_id = " + idDelete, null);
                refreshList();
                mDataBase.close();
            }
        });

        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public void show() {

        final Dialog d = new Dialog(getActivity());
        d.setTitle("Меню: ");
        d.setContentView(R.layout.dialog_menu);

        Button btnInfo = (Button) d.findViewById(R.id.btnInfo);
        Button bntDelete = (Button) d.findViewById(R.id.bntDelete);
        Button bntMinus = (Button) d.findViewById(R.id.bntMinus);
        Button bntPlus = (Button) d.findViewById(R.id.bntPlus);
        amountEditTextDialog = (EditText) d.findViewById(R.id.amountEditText);
        Button btnOkDialog = (Button) d.findViewById(R.id.btnOkDialog);

        String priceSplit [] = amount.split("x");
        amount = priceSplit[0];
        amountEditTextDialog.setText(amount);

        btnOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String getTextAmount = amountEditTextDialog.getText().toString();
                int intAmount = Integer.parseInt(getTextAmount);
                getTextAmount = String.valueOf(intAmount);

                ContentValues cv = new ContentValues();
                mDataBase = dbHelper.getWritableDatabase();


                String idof = "_id = " + idSting;

                cv.put("amount", getTextAmount);
                mDataBase.update("orderTable", cv, idof, null);
                mDataBase.close();

                d.dismiss();

                refreshList();
            }
        });

        bntMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String getTextAmount = amountEditTextDialog.getText().toString();
                int intAmount = Integer.parseInt(getTextAmount);

                if (intAmount > 1) {
                    --intAmount;
                }
                getTextAmount = String.valueOf(intAmount);
                amountEditTextDialog.setText(getTextAmount);
            }
        });

        bntPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String getTextAmount = amountEditTextDialog.getText().toString();
                int intAmount = Integer.parseInt(getTextAmount);

                if (intAmount < 29) {
                    ++intAmount;
                }
                getTextAmount = String.valueOf(intAmount);
                amountEditTextDialog.setText(getTextAmount);
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("idOrder", idSting);
                bundle.putString("nameBasket", idName);
                bundle.putString("idMenu", idMenuSting);
                bundle.putInt("key", 1);

                SummaryItemFragment summaryItemFragment = new SummaryItemFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                summaryItemFragment.setArguments(bundle);

                ft.replace(R.id.frgmCont, summaryItemFragment, "SummaryItemFragmentBasket").addToBackStack(null);

                ft.commit();

                d.dismiss();
            }
        });

        bntDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dbHelper = new DBHelper(getActivity());
                mDataBase = dbHelper.getWritableDatabase();
                mDataBase.delete("orderTable", "_id = " + idDelete, null);
                refreshList();
                mDataBase.close();
                d.dismiss(); // dismiss the dialog
            }
        });

        d.show();
    }

}