package com.epon.fragments;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.epon.DataBase.DBHelper;
import com.epon.DataBase.MyData;
import com.epon.DataBase.Product;
import com.epon.DataBase.helpers.ProductHelper;
import com.epon.EponApp;
import com.epon.MainActivity.MainActivity;
import com.epon.R;
import com.epon.adapters.DBAdapter;
import com.epon.adapters.GridAdapter;
import com.epon.adapters.ProductAdapter;

import java.util.ArrayList;


public class SubCategory extends Fragment {

    static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_LOGO = "logo";

    private TextView textOfSub;

    FillTheList fillTheList;
    ViewPager mPager;

    //Rotation
    View rootView;
    private ListView listView;
    private GridView gridView;
    private String currentPosition;
    private String idSting;
    private GridAdapter gridAdapter;

    //DB
    DBAdapter dbAdaper;
    DBHelper dbHelper;
    SQLiteDatabase mDataBase;

    //Bundle
    String name;
    String categoryIntent;
    String typeIntent;
    int category_id;
    int tag_id;
    ProductHelper productHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        if (getRotation(getActivity()) == "landscape") {

            rootView = inflater.inflate(R.layout.fragment_grid_sushi,
                    container, false);
            gridView = (GridView) rootView.findViewById(R.id.gridView1);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //@Override
                public void onItemClick(AdapterView arg0, View v,
                                        int position, long id) {
                    currentPosition = ((TextView) v.findViewById(R.id.nameOfCafe)).getText().toString(); //Get name
                    idSting = ((TextView) v.findViewById(R.id.idOfSushiMenu)).getText().toString(); //Get id
                    goTonextFragment();
                }
            });

        } else {

            rootView = inflater.inflate(R.layout.fragment_list_sushi,
                    container, false);
            listView = (ListView) rootView.findViewById(android.R.id.list);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //@Override
                public void onItemClick(AdapterView arg0, View v,
                                        int position, long id) {
                    mPager.setVisibility(View.GONE);
                    currentPosition = ((TextView) v.findViewById(R.id.nameOfCafe)).getText().toString(); //Get name
                    idSting = ((TextView) v.findViewById(R.id.idOfSushiMenu)).getText().toString(); //Get id
                    goTonextFragment();
                }
            });
        }

        mPager = (ViewPager) getActivity().findViewById(R.id.pager);

        //Getting of Element's id
        if (rootView != null) {
            textOfSub = (TextView) rootView.findViewById(R.id.textCho);
        }

        name = getArguments().getString("name");
        categoryIntent = getArguments().getString("category");
        typeIntent = getArguments().getString("type");
        category_id= getArguments().getInt("category_id");
        tag_id= getArguments().getInt("tag_id");
        if (name != null) {
                textOfSub.setText(name.toUpperCase());
        }

        //DB
        dbHelper = new DBHelper(getActivity());
        productHelper= EponApp.getInstance().getAllHelper().getProductHelper();

        return rootView;
    }

    private void goTonextFragment() {

        mPager.setVisibility(View.GONE);
        Bundle bundle = new Bundle();
        bundle.putString("name", currentPosition);
        bundle.putString("id", idSting);
        bundle.putString("parent",name);
        SummaryItemFragment summaryItemFragment = new SummaryItemFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        summaryItemFragment.setArguments(bundle);
        ft.replace(R.id.frgmCont, summaryItemFragment, "SummaryItemFragment");
        ft.addToBackStack(null);
        ((MainActivity)getActivity()).setSummaryItemFragment();
        /*if (rotation.equalsIgnoreCase("portrait") || rotation.equalsIgnoreCase("reverse portrait")) {
            ft.replace(R.id.frgmCont, summaryItemFragment, "SummaryItemFragment");

        } else {

            ft.replace(R.id.cont, summaryItemFragment, "SummaryItemFragment");
        }*/
        ft.commit();
    }

    class FillTheList extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            ArrayList<Product> products=productHelper.getProductsByTag(tag_id,category_id);
            if (getRotation(getActivity()) == "landscape") {
                /*gridAdapter = new GridAdapter(getActivity(), selectAll());
                gridView.setAdapter(gridAdapter);*/
                ProductAdapter productAdapter= new ProductAdapter(getActivity(),products,EponApp.LANDSCAPE);
                gridView.setAdapter(productAdapter);


            } else {
               /* dbAdaper = new DBAdapter(getActivity(), selectAll());
                listView.setAdapter(dbAdaper);
                dbAdaper.notifyDataSetChanged();*/
                ProductAdapter productAdapter= new ProductAdapter(getActivity(),products,EponApp.PORTRAIT);
                listView.setAdapter(productAdapter);
            }

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    } //End of FillTheList


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Listeners-----------------------------------------------

        fillTheList = new FillTheList();
        fillTheList.execute();
    }

    @Override
    public void onPause() {
        super.onPause();
        fillTheList.cancel(false);
    }

    public ArrayList<MyData> selectAll() {

        mDataBase = dbHelper.getReadableDatabase();
        Cursor mCursor = mDataBase.query("menuTable", null, null, null, null, null, null);

        ArrayList<MyData> arr = new ArrayList<MyData>();
        ArrayList<String> list1 = new ArrayList<String>();

        mCursor.moveToFirst();
        if (!mCursor.isAfterLast()) {
            do {

                String subcategory = mCursor.getString(mCursor
                        .getColumnIndex("subcategory"));

                String category = mCursor.getString(mCursor
                        .getColumnIndex("category"));

                String type = mCursor.getString(mCursor
                        .getColumnIndex("type"));

                if (subcategory.equalsIgnoreCase(name) && category.equalsIgnoreCase(categoryIntent)
                        && type.equalsIgnoreCase(typeIntent)) {
                    String name = mCursor.getString(mCursor
                            .getColumnIndex("name"));
                    String url = mCursor.getString(mCursor
                            .getColumnIndex("url"));
                    int id = mCursor.getInt(mCursor.getColumnIndex("_id"));

                    arr.add(new MyData(id, null, null, name, null, null, null, null, null, url));
                    list1.add(name);
                }

            } while (mCursor.moveToNext());
        }

        mCursor.close();
        mDataBase.close();
        return arr;
    }

    public String getRotation(Context context){
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return "portrait";
            case Surface.ROTATION_90:
                return "landscape";
            case Surface.ROTATION_180:
                return "reverse portrait";
            default:
                return "reverse landscape";
        }
    }
}




