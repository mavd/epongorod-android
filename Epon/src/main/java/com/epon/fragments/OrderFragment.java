/*package com.epon.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.epon.DataBase.DBHelper;

import com.epon.MainActivity.DateTimePicker;
import com.epon.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class OrderFragment extends Activity implements View.OnClickListener, DateTimePicker.ICustomDateTimeListener {

    private DateTimePicker dateTimePicker;

    //Radio
    private RadioGroup radioCity;
    private RadioButton radioCityButton;
    private RadioGroup radioDelev;
    private RadioButton radioDelevButton;

    private CheckBox chkBox;

    private Spinner spinerDist;

    //ID's of elements
    private EditText editTime1;
    private EditText editTextName;
    private EditText editTextTelephone;
    private EditText editTextAddress;
    private EditText editTextAmountOfPers;
    private EditText editTextAdditional;

    String messageText;

    ImageButton button;
    Button makeAnorder;

    //DB
    private DBHelper dbHelper;
    private SQLiteDatabase mDataBase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_order);

        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextTelephone = (EditText) findViewById(R.id.editTextTelephone);
        editTextAddress = (EditText) findViewById(R.id.editTextAddress);
        editTextAdditional = (EditText) findViewById(R.id.editTextAdditional);

        chkBox = (CheckBox) findViewById(R.id.chkBox);
        spinerDist = (Spinner)findViewById(R.id.spinnertDist);

        radioCity = (RadioGroup) findViewById(R.id.radioCity);
        radioDelev = (RadioGroup) findViewById(R.id.radioDeliver);

        button = (ImageButton) findViewById(R.id.timeBtn);
        button.setOnClickListener(this);

        makeAnorder = (Button) findViewById(R.id.makeAnorder);
        makeAnorder.setOnClickListener(this);

        editTime1.setVisibility(View.INVISIBLE);
        button.setEnabled(false);

        chkBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {

                    editTime1.setVisibility(View.VISIBLE);
                    button.setEnabled(true);

                } else {
                    editTime1.setVisibility(View.INVISIBLE);
                    button.setEnabled(false);
                }

            }
        });

        //Spinner Adapter

        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(this, R.array.spinerList, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerDist.setAdapter(adapter);

    }

    //Method of Date and time picker
    @Override
    public void onSet(Calendar calendarSelected, Date dateSelected, int year,
                      String monthFullName, String monthShortName,
                     int monthNumber,
                      int date, String weekDayFullName, String weekDayShortName,
                      int hour24, int hour12, int min, int sec, String AM_PM) {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //yyyy-MM-dd HH:mm:ss
        String strDate = sdfDate.format(dateSelected);
        editTime1.setText(strDate);
    }

    @Override
    public void onCancel() {

        Log.d("datetimepickerdialog", "canceled");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.timeBtn:
            //Obtaining Address name From
            dateTimePicker = new DateTimePicker(OrderFragment.this, this);
            dateTimePicker.set24HourFormat(true);
            dateTimePicker.showDialog();
            break;

            case R.id.makeAnorder:

                if (editTextName.getText().toString().equalsIgnoreCase("")
                        || editTextTelephone.getText().toString().equalsIgnoreCase("")) {
                    Toast toast = Toast.makeText(this, "Введите Имя и номер телефона", Toast.LENGTH_SHORT);
                    toast.show();

                } else {


                    //City radio
                    int selectedId = radioCity.getCheckedRadioButtonId();
                    radioCityButton = (RadioButton) findViewById(selectedId);

                    //Delev radio
                    int selectedId2 = radioDelev.getCheckedRadioButtonId();
                    radioDelevButton = (RadioButton) findViewById(selectedId2);

                    messageText = "Город: " + radioCityButton.getText().toString() + "\n" +
                            "Имя: " + editTextName.getText().toString() + "\n" +
                            "Телефон: "+ editTextTelephone.getText().toString() + "\n" +
                            "Район: " + spinerDist.getSelectedItem().toString() + "\n" +
                            "Адрес: " + editTextAddress.getText().toString() + "\n" +
                            "Кол-во персон: " + editTextAmountOfPers.getText().toString() + "\n" +
                            "Способ доставки: " + radioDelevButton.getText().toString() + "\n" +
                            "Коммент.: " + editTextAdditional.getText().toString() + "\n" +
                            "Время: " + editTime1.getText().toString() + "\n" +
                            "------------------------------------" + "\n" +
                            "Выбранные блюда: " + "\n";


                    Cursor mCursor = null;
                    dbHelper = new DBHelper(this);
                    mDataBase = dbHelper.getReadableDatabase();
                    mCursor = mDataBase.query("orderTable", null, null, null, null, null, null);

                    mCursor.moveToFirst();
                    if (!mCursor.isAfterLast()) {
                        do {

                            String name = mCursor.getString(mCursor
                                    .getColumnIndex("name"));
                            String amount = mCursor.getString(mCursor
                                    .getColumnIndex("amount"));
                            messageText += "Название: " + name + ", Кол-во: " + amount + "\n";

                        } while (mCursor.moveToNext());
                    }

                    mCursor.close();
                    mDataBase.close();


                    Intent email = new Intent(Intent.ACTION_SEND);
                    email.putExtra(Intent.EXTRA_EMAIL, new String[]{ "epon@epon.ru"});
                    email.putExtra(Intent.EXTRA_SUBJECT, "Заказ");
                    email.putExtra(Intent.EXTRA_TEXT, messageText);

                    //need this to prompts email client only
                    email.setType("message/rfc822");

                    startActivity(Intent.createChooser(email, "Выбор Email-клиента :"));
                }
        }
    }
}*/