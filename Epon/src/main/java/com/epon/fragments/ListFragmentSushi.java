package com.epon.fragments;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.epon.DataBase.Category;
import com.epon.DataBase.DBHelper;
import com.epon.DataBase.MyData;
import com.epon.DataBase.helpers.AllHelper;
import com.epon.DataBase.helpers.CategoryHelper;
import com.epon.EponApp;
import com.epon.R;
import com.epon.adapters.CategoryAdapter;
import com.epon.adapters.DBAdapter;
import com.epon.adapters.GridAdapter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


public class ListFragmentSushi extends Fragment {

    static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_LOGO = "logo";

    //Rotation
    View rootView;

    private String currentPosition;

    private ListView listView;
    private GridView gridView;

    private FillTheList fillTheList;
    private ViewPager mPager;
    //DB
    private DBAdapter dbAdaper;
    private DBHelper dbHelper;
    private SQLiteDatabase mDataBase;
    private GridAdapter gridAdapter;
    private CategoryHelper categoryHelper;
    private String idSting;
    public static final String TAG_ID="TAG_ID";
    public static final String PARENT_ID="PARENT_ID";
    int tag_id;
    int parent_id;

    public static ListFragmentSushi newInstance(int tag, int parent) {
        ListFragmentSushi pageFragment = new ListFragmentSushi();
        Bundle arguments = new Bundle();
        arguments.putInt(TAG_ID, tag);
        arguments.putInt(PARENT_ID, parent);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (getRotation(getActivity()) == "landscape") {

            rootView = inflater.inflate(R.layout.fragment_grid_sushi,
                    container, false);

            if (rootView != null) {
                gridView = (GridView) rootView.findViewById(R.id.gridView1);
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    //@Override
                    public void onItemClick(AdapterView arg0, View v,
                                            int position, long id) {
                        idSting = ((TextView) v.findViewById(R.id.idOfSushiMenu)).getText().toString(); //Get id
                        currentPosition = ((TextView) v.findViewById(R.id.nameOfCafe)).getText().toString(); //Get name
                        goTonextFragment();
                    }
                });
            }


        } else {

            rootView = inflater.inflate(R.layout.fragment_list_sushi,
                    container, false);
            listView = (ListView) rootView.findViewById(android.R.id.list);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //@Override
                public void onItemClick(AdapterView arg0, View v,
                                        int position, long id) {
                    mPager.setVisibility(View.GONE);
                    idSting = ((TextView) v.findViewById(R.id.idOfSushiMenu)).getText().toString(); //Get id
                    currentPosition = ((TextView) v.findViewById(R.id.nameOfCafe)).getText().toString(); //Get name
                    goTonextFragment();
                }
            });
        }

        mPager = (ViewPager) getActivity().findViewById(R.id.pager);

        //DB
        dbHelper = new DBHelper(getActivity());
        categoryHelper= EponApp.getInstance().getAllHelper().getCategoryHelper();
        fillTheList = new FillTheList();
        fillTheList.execute();
        tag_id=getArguments().getInt(TAG_ID);
        parent_id=getArguments().getInt(PARENT_ID);
        return rootView;
    }

    private void goTonextFragment() {

        mPager.setVisibility(View.GONE);

        Bundle bundle = new Bundle();
        bundle.putString("name", currentPosition);
        bundle.putString("category", "Основное Меню");
        bundle.putString("type", "Епонский Городовой");
        bundle.putInt("category_id",Integer.parseInt(idSting));
        bundle.putInt("tag_id",tag_id);
        SubCategory frag2 = new SubCategory();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        frag2.setArguments(bundle);
        ft.replace(R.id.frgmCont, frag2, "SubCategoryFragment");
        ft.addToBackStack(null);
        ft.commit();
    }
    /*public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        mPager.setVisibility(View.GONE);

        String currentPosition = ((TextView) v.findViewById(R.id.nameOfCafe)).getText().toString(); //Get name
        Bundle bundle = new Bundle();
        bundle.putString("name", currentPosition);
        bundle.putString("category", "Основное Меню");
        bundle.putString("type", "Епонский Городовой");
        SubCategory frag2 = new SubCategory();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        frag2.setArguments(bundle);
        ft.replace(R.id.frgmCont, frag2, "SubCategoryFragment");
        ft.addToBackStack(null);
        ft.commit();

    }*/

    class FillTheList extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

           return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            //ArrayList<Category> categories= categoryHelper.getCategoriesForTag(EponApp.getInstance().id_usuall,EponApp.getInstance().id_epon);
            ArrayList<Category> categories= categoryHelper.getCategoriesForTag(tag_id,parent_id);
            if (getRotation(getActivity()) == "landscape") {
                /*gridAdapter = new GridAdapter(getActivity(), selectAll());
                gridView.setAdapter(gridAdapter);*/
                CategoryAdapter categoryAdapter= new CategoryAdapter(getActivity(),categories,EponApp.LANDSCAPE,tag_id);
                gridView.setAdapter(categoryAdapter);
            } else {
                /*dbAdaper = new DBAdapter(getActivity(), selectAll());
                listView.setAdapter(dbAdaper);
                dbAdaper.notifyDataSetChanged();*/
                CategoryAdapter categoryAdapter= new CategoryAdapter(getActivity(),categories,EponApp.PORTRAIT,tag_id);
                listView.setAdapter(categoryAdapter);
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    } //End of FillTheList

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        fillTheList.cancel(false);
    }

    public ArrayList<MyData> selectAll() {

        Cursor mCursor = null;
        mDataBase = dbHelper.getReadableDatabase();
        mCursor = mDataBase.query("menuTable", null, null, null, null, null, null);

        ArrayList<MyData> arr = new ArrayList<MyData>();
        //ArrayList<String> list1 = new ArrayList<String>();
        List<String> list1 = new ArrayList<String>();
        ArrayList<String> sortList = new ArrayList<String>();

        mCursor.moveToFirst();

        //Street magic
        if (!mCursor.isAfterLast()) {
            do {

                String type = mCursor.getString(mCursor
                        .getColumnIndex("type"));
                String category = mCursor.getString(mCursor
                        .getColumnIndex("category"));

                if (type.equalsIgnoreCase("Епонский Городовой") && category.equalsIgnoreCase("Основное Меню")) {
                    String name = mCursor.getString(mCursor
                            .getColumnIndex("subcategory"));
                    list1.add(name);
                }

            } while (mCursor.moveToNext());
        }

        List<String> list2 = new ArrayList<String>();
        HashSet<String> lookup = new HashSet<String>();
        for (String item : list1) {
            if (lookup.add(item)) {
                // Set.add returns false if item is already in the set
                list2.add(item);
            }
        }
        list1 = list2;

        for (String value : list1) {

            mCursor.moveToFirst();
            if (!mCursor.isAfterLast()) {
                do {

                    String type = mCursor.getString(mCursor
                            .getColumnIndex("subcategory"));

                    if (type.equalsIgnoreCase(value)) {
                        String url = mCursor.getString(mCursor
                                .getColumnIndex("url"));
                        int id = mCursor.getInt(mCursor.getColumnIndex("_id"));

                        if (!sortList.contains(type)) {
                            sortList.add(type);
                            arr.add(new MyData(id, null, null, value, null, null, null, null, null, url));
                        }
                    }

                } while (mCursor.moveToNext());
            }
        }
        return arr;
    }//End of street magic

    public String getRotation(Context context){
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return "portrait";
            case Surface.ROTATION_90:
                return "landscape";
            case Surface.ROTATION_180:
                return "reverse portrait";
            default:
                return "reverse landscape";
        }
    }
}