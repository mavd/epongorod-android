package com.epon.fragments;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.epon.DataBase.DBHelper;
import com.epon.DataBase.Product;
import com.epon.DataBase.helpers.AllHelper;
import com.epon.DataBase.helpers.ImageHelper;
import com.epon.DataBase.helpers.ProductHelper;
import com.epon.EponApp;
import com.epon.MainActivity.MainActivity;
import com.epon.R;
import com.epon.lazyList.ImageLoader;
import com.epon.network.ImageRequest;

import java.io.InputStream;

public class SummaryItemFragment extends Fragment {

    private String name;
    double dblprice;
    int id;
    int key;
    //Basket
    String idStringOrder;
    protected int WeightDiam = 0;
    //DB
    private DBHelper dbHelper;
    private SQLiteDatabase mDataBase;

    //IDs of elements
    private TextView textSummary;
    private TextView textSummary1;
    private TextView textPrice;
    private TextView textWeight;
    private ImageView imageLogo;
    private Button orderButton;
    private TextView amountEditText;
    private Button minusBtn;
    private Button plusBtn;

    //Strings
    private String composition;
    private String weight;
    private String price;
    private String url;
    private String parent;
    private MyTask mt;
    private ProductHelper productHelper;
    ImageHelper imageHelper;
    private ImageLoader imageLoader;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.summary_item,
                container, false);
        imageHelper=EponApp.getInstance().getAllHelper().getImageHelper();
        textSummary = (TextView) rootView.findViewById(R.id.textAbout);
        textSummary1 = (TextView) rootView.findViewById(R.id.textSummary);
        textPrice = (TextView) rootView.findViewById(R.id.textPrice);
        textWeight = (TextView) rootView.findViewById(R.id.textWeight);
        imageLogo = (ImageView) rootView.findViewById(R.id.imageLogo);
        orderButton = (Button) rootView.findViewById(R.id.orderButton);
        amountEditText = (TextView) rootView.findViewById(R.id.amountText);
        minusBtn = (Button) rootView.findViewById(R.id.bntMinus);
        plusBtn = (Button) rootView.findViewById(R.id.bntPlus);
        //Number picker
        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getTextAmount = amountEditText.getText().toString();
                int intAmount = Integer.parseInt(getTextAmount);

                if (intAmount > 1) {
                    --intAmount;
                }
                getTextAmount = String.valueOf(intAmount);
                amountEditText.setText(getTextAmount);
            }
        });

        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String getTextAmount = amountEditText.getText().toString();
                int intAmount = Integer.parseInt(getTextAmount);

                if (intAmount < 29) {
                    ++intAmount;
                }
                getTextAmount = String.valueOf(intAmount);
                amountEditText.setText(getTextAmount);
            }
        });
        //imageLoader
        imageLoader= new ImageLoader(getActivity());

        //DB
        dbHelper = new DBHelper(getActivity());

        //Bundle
        key = getArguments().getInt("key");

        if (key == 1) { //BasketFragment
            idStringOrder = getArguments().getString("idOrder");
            name = getArguments().getString("nameBasket");
            String idMenu = getArguments().getString("idMenu");
            id = Integer.parseInt(idMenu);

        } else { //SubCategory
            String idString = getArguments().getString("id");
            name = getArguments().getString("name");
            id = Integer.parseInt(idString);
        }
        parent=getArguments().getString("parent");
        productHelper= EponApp.getInstance().getAllHelper().getProductHelper();
        mt = new MyTask();
        mt.execute();

        //Add to Basket button
        View.OnClickListener oclBtnOk = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (key == 1) {
                    upDateFragment();

                } else {

                    ContentValues cv = new ContentValues();
                    mDataBase = dbHelper.getReadableDatabase();
                    String amount = amountEditText.getText().toString();
                    Cursor mCursor = mDataBase.rawQuery("select 1 from orderTable where name=?", new String[] {name});
                    boolean exists = (mCursor.getCount() > 0);
                    if (exists) {
                        if (amount.equals("")) {
                            amount = "1";
                        }
                        ContentValues cv2 = new ContentValues();
                        cv2.put("amount", amount);
                        mDataBase = dbHelper.getWritableDatabase();
                        mDataBase.update("orderTable", cv2, "name" + " = '" + name + "'", null);
                        Toast.makeText(getActivity(),"Данные обновлены!",Toast.LENGTH_SHORT).show();

                    } else {

                        if (amount.equals("")) {
                            amount = "1";
                        }
                        try {
                            dblprice = Double.valueOf(price.trim());

                        } catch (NumberFormatException nfe) {
                            dblprice = 1;
                        }
                        double amountDbl = Double.valueOf(amount);

                        dblprice = amountDbl * dblprice;
                        cv.put("name", name);
                        cv.put("amount", amount);
                        cv.put("price", price);
                        cv.put("menuId", id);
                        cv.put("url", url);
                        cv.put("desc",composition);
                        cv.put("subcategory",parent);
                        mDataBase = dbHelper.getWritableDatabase();
                        mDataBase.insert("orderTable", null, cv);
                        Toast.makeText(getActivity(),"Добавлено в корзину!",Toast.LENGTH_SHORT).show();
                    }

                    String summ = getSumm();
                    ((MainActivity)getActivity()).setCostToActionBar((int)Double.parseDouble(summ));
                    TextView bas = (TextView) getActivity().findViewById(R.id.priceOrderText);
                    if (bas != null) {
                        bas.setText(summ);
                    }/* else {
                        BasketFragment basketFragment = new BasketFragment();
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.frgmCont, basketFragment, "BasketFragment").addToBackStack(null);
                        ft.commit();
                    }*/

                    mCursor.close();
                    mDataBase.close();
                }
            }
        };

        orderButton.setOnClickListener(oclBtnOk);

        return rootView;

    }

    class MyTask extends AsyncTask<Void, Void, Product> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Product doInBackground(Void... params) {
            Product product= productHelper.getProductById(id);
           /* mDataBase = dbHelper.getReadableDatabase();
            Cursor mCursor;

            if (mDataBase != null) {
                //mCursor = mDataBase.query("menuTable", null, null, null, null, null, null);
                mCursor = mDataBase.rawQuery("SELECT * FROM menuTable WHERE _id = " + id + "", null);
                mCursor.moveToFirst();

                if (mCursor.moveToFirst()) {

                    do {
                        int idCurs = mCursor.getInt(mCursor.getColumnIndex("_id"));

                        if (id == idCurs) {
                            composition = mCursor.getString(mCursor
                                    .getColumnIndex("composition"));
                            weight = mCursor.getString(mCursor
                                    .getColumnIndex("weight"));

                            if (weight.equalsIgnoreCase("")) {
                                weight = mCursor.getString(mCursor
                                        .getColumnIndex("diameter"));
                                WeightDiam = 1;
                            }

                            price = mCursor.getString(mCursor
                                    .getColumnIndex("price"));
                            url = mCursor.getString(mCursor
                                    .getColumnIndex("url"));
                        }

                    } while (mCursor.moveToNext());
                    mCursor.close();
                }
            }*/

            return product;
        }

        @Override
        protected void onPostExecute(Product result) {
            super.onPostExecute(result);

            /*ImageLoader imageLoader = new ImageLoader(getActivity());
            imageLoader.DisplayImageinSummaryItem(url, imageLogo);
            imageLoader.clearCache();*/
            //new DownloadImageTask(imageLogo).execute(imageHelper.getUrlByTitleAndCategory(ImageRequest.PRODUCT_TYPE,result.photo));
            //if (WeightDiam == 1) {
               // textWeight.setText("ДИАМЕТР: " + weight);

            //} else if (WeightDiam == 0)
            if (getActivity()!=null) {
                imageLoader.DisplayImage(imageHelper.getUrlByTitleAndCategory(ImageRequest.PRODUCT_TYPE, result.photo), imageLogo, false,
                        getResources().getDimensionPixelSize(R.dimen.main_photo_img_size));
            }
            weight=Integer.toString(result.weight);
            textWeight.setText("ВЕС: " + weight + " гр.");
            /*} else {
                textWeight.setText("");
            }*/
            url=result.photo;
            price = Integer.toString(result.cost);
            composition=result.description.replace("\\","");
            textSummary.setText(result.description.replace("\\",""));
            textPrice.setText(" " + Integer.toString(result.cost) + " р.");
            //textSummary1.setText(name);
            amountEditText.setText(Integer.toString(EponApp.getInstance().getAmountOfOrder(name)));
            dbHelper.close();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }



    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if (getActivity()!=null)
                if (isAdded()) {
                    bmImage.setImageBitmap(result);
                    bmImage.setBackgroundColor(getResources().getColor(R.color.light_text));
                }
        }
    }

    public String getRotation(Context context){
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return "portrait";
            case Surface.ROTATION_90:
                return "landscape";
            case Surface.ROTATION_180:
                return "reverse portrait";
            default:
                return "reverse landscape";
        }
    }

    public void upDateFragment() {

        ContentValues cv = new ContentValues();
        mDataBase = dbHelper.getWritableDatabase();
        String amount = amountEditText.getText().toString();

        String idof = "_id = " + idStringOrder;

        cv.put("amount", amount);
        mDataBase.update("orderTable", cv, idof, null);
        mDataBase.close();

        String rotation = getRotation(getActivity());
        if (rotation.equalsIgnoreCase("landscape")) {
            BasketFragment basketFragment = new BasketFragment();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.frgmCont, basketFragment, "BasketFragment").addToBackStack(null);
            ft.commit();
        }

        Toast.makeText(getActivity(),"Данные обновлены!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity()!=null) {
            ((MainActivity) getActivity()).setmActionBarTitle(name);
        }
    }

    public String getSumm() {
        dbHelper = new DBHelper(getActivity());
        mDataBase = dbHelper.getReadableDatabase();
        Cursor mCursor = mDataBase.query("orderTable", null, null, null, null, null, null);
        double dblSumm = 0;
        double dblprice;
        mCursor.moveToFirst();
        if (!mCursor.isAfterLast()) {
            do {

                String amount = mCursor.getString(mCursor
                        .getColumnIndex("amount"));
                String price = mCursor.getString(mCursor
                        .getColumnIndex("price"));

                try {
                    dblprice = Double.valueOf(price);

                } catch (NumberFormatException nfe) {
                    dblprice = 1;
                }
                double amountInt = Double.valueOf(amount);
                amountInt = dblprice*amountInt;

                dblSumm +=amountInt;

            } while (mCursor.moveToNext());
        }

        //Set Summ
        String summString = String.valueOf(dblSumm);

        return summString;
    }
}