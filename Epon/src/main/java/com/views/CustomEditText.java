package com.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomEditText extends EditText {

	public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
 
    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
	
    public CustomEditText(Context context) {
		super(context);
	}

	@Override
    public void onSelectionChanged(int start, int end) {

        CharSequence text = getText();
        if (text != null && ((text.toString().contentEquals("_-___-___-__-__") || text.toString().contentEquals("__-___-___-__-__")))) {
            if (start != 0 || end != 0) {
                setSelection(0, 0);
                return;
            }
        }

        super.onSelectionChanged(start, end);
    }

}