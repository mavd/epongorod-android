package com.views;

import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;

public class PhoneNumberTextWatcher implements TextWatcher {

	EditText view;
	
	public OnFocusChangeListener onFocusChangeListener = new OnFocusChangeListener() {

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			// TODO Auto-generated method stub
			if(v.equals(mPhoneField)){
				view = (EditText) v;
				setupPhoneField();
			}
			else {
				clearPhoneField();
			}
		}


	};

	public OnTouchListener onPhoneFieldTouchListener = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if  (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_DOWN)
				view = (EditText) v;
				setupPhoneField();
//				final int eventX = event.getX();
//		        final int eventY = event.getY();
//		        if( (eventX,eventY) is in the middle of your editText)
//		         {
//		              return false;
//		         }
//		         return true;
			return false;
		}
	};

	public OnClickListener onPhoneFieldClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(v.equals(mPhoneField)){
				view = (EditText) v;
				setupPhoneField();
		}
		}
	};

	public void setupPhoneField() {
		String text = mPhoneField.getText().toString();

		if (text.isEmpty()) {
			isFirstTimeLaunched = true;
			text = "__-___-___-__-__";
			mPhoneField.setText(text);
			Editable etext = mPhoneField.getText();

			Selection.setSelection(etext, 1, 1);
			etext.delete(0, 1);
			view.requestFocus();
			view.setSelection(0);
		} 
	}

	public void clearPhoneField() {
		String text = mPhoneField.getText().toString();
		if(text.contentEquals("_-___-___-__-__")){
			isFirstTimeLaunched = true;
			mPhoneField.setText("");
		}

	}

	public OnKeyListener onPhoneFieldKeyListener = new OnKeyListener() {

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			String text = mPhoneField.getText().toString();

			if(keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.ACTION_DOWN){
				if(text.contentEquals("_-___-___-__-__")){
					mPhoneField.setText("");
				}

			}

			return false;
		}
	};

	String inputText = "_-___-___-__-__";
	String currentInputText = "";
	int maxLength = -1;
	char hiphen = '-';
	private boolean mFormatting;
	private int selectionStart = -1;
	private boolean mIsNeedToDeleteHyphen;
	private char newCharacter = 'e';
	private char oldCharacter = 'e';
	private char deletedCharacter = 'e';
	public boolean isFirstTimeLaunched = true;
	static String previousString = "";
	static String newString = "";
	private static EditText mPhoneField;
	public PhoneNumberTextWatcher(EditText phoneField) {
		mPhoneField = phoneField;
		isFirstTimeLaunched = true;
	}

	public synchronized void afterTextChanged(Editable text) {
		if(selectionStart != -1){
			if(mIsNeedToDeleteHyphen){
				mIsNeedToDeleteHyphen = false;
				Editable etext = mPhoneField.getText();
				Selection.setSelection(etext, selectionStart - 1);
			} else if (mFormatting){
				mFormatting = false;
				Editable etext = mPhoneField.getText();
				Selection.setSelection(etext, selectionStart);
			}

		}
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		previousString = s.toString();

	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		newString = s.toString();	
		if(!isFirstTimeLaunched){
			if(newString.length() > 15){
				int numberOfHyphen = calculateHyphens();
				if(numberOfHyphen > 4){
					mIsNeedToDeleteHyphen = true;
					selectionStart = mPhoneField.getSelectionStart();

					mPhoneField.setText(previousString);	

				} else {

					newCharacter = s.charAt(start);
					if(start < previousString.length()){
						oldCharacter = previousString.charAt(start);
						if(Character.isDigit(newCharacter)){

							mFormatting = true;
							StringBuilder stringBuilder = new StringBuilder(previousString);
							if(oldCharacter != '-'){
								selectionStart = mPhoneField.getSelectionStart();
								stringBuilder.setCharAt(start, newCharacter);
							} else {
								selectionStart = mPhoneField.getSelectionStart() + 1;
								stringBuilder.setCharAt(start + 1, newCharacter);
							}
							oldCharacter = newCharacter;
							mPhoneField.setText(stringBuilder.toString());
						}
					} else {
						mFormatting = true;
						selectionStart = start;
						mPhoneField.setText(previousString);
					}

				}
			} else if(newString.length() < 15){
				mFormatting = true;
				deletedCharacter = previousString.charAt(start);
				StringBuilder stringBuilder = new StringBuilder(previousString);

				if(deletedCharacter != '-'){
					selectionStart = mPhoneField.getSelectionStart();
					stringBuilder.setCharAt(start, '_');
				} else {
					selectionStart = mPhoneField.getSelectionStart() - 1;
					stringBuilder.setCharAt(start - 1, '_');
				}

				mPhoneField.setText(stringBuilder.toString());
			}
		} else {
			isFirstTimeLaunched = false;
		}
	}

	private int calculateHyphens() {
		int numberOfHyphen = 0;
		for(int i = 0; i < newString.length() ; i ++){
			char character = newString.charAt(i);
			if(character == '-'){
				numberOfHyphen++;
			}
		}
		return numberOfHyphen;
	}

}
