package com.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by macbook on 13/01/15.
 */
public class LinedEditText extends TextView {
    private Rect mRect;
    private Paint mPaint;
    String text;

    public LinedEditText(Context context, AttributeSet attrs, String text) {
        super(context, attrs);
        this.text = text;

        mRect = new Rect();
        mPaint = new Paint();
        mPaint.setARGB(255, 0, 0, 0);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setPathEffect(new DashPathEffect(new float[] { 10, 10 }, 0));
    }
    public LinedEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.text = "Комментарий";

        mRect = new Rect();
        mPaint = new Paint();
        mPaint.setARGB(255, 0, 0, 0);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setPathEffect(new DashPathEffect(new float[] { 10, 10 }, 0));
    }
    public LinedEditText(Context context) {
        super(context);
        this.text = "Комментарий";

        mRect = new Rect();
        mPaint = new Paint();
        mPaint.setARGB(255, 0, 0, 0);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setPathEffect(new DashPathEffect(new float[] { 10, 10 }, 0));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Rect r = mRect;
        Paint paint = mPaint;

        int lineCount = getLineCount();
        int size = getLayout().getLineStart(lineCount-1);

        String str = getText().toString().substring(size);

        float densityMultiplier = getContext().getResources().getDisplayMetrics().density;
        float scaledPx = 20 * densityMultiplier;
        paint.setTextSize(scaledPx);
        float i = paint.measureText(str);

        for (int k = 0; k < lineCount-1; k++) {
            int baseline = getLineBounds(k, r);
            canvas.drawLine(r.left, baseline + 2, r.right, baseline + 2, paint);
        }

        int baseline = getLineBounds(lineCount-1, r);
        canvas.drawLine(r.left, baseline + 2, i, baseline + 2, paint);

        super.onDraw(canvas);
    }
}