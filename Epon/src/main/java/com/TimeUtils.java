package com;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by macbook on 30/11/14.
 */
public class TimeUtils {
    public static String formatTime(long time){
        if (time==0){
            time=System.currentTimeMillis();
        }
        Date date=new Date(time);
        Locale locale = new Locale("ru","RU");
        SimpleDateFormat dateFormat =new SimpleDateFormat("dd MMMM yyyy, HH:mm", locale);
        return dateFormat.format(date);
    }

    public static String getDate(long time){
        Date date=new Date(time);
        Locale locale = new Locale("ru","RU");
        SimpleDateFormat dateFormat =new SimpleDateFormat("dd MMMM yyyy", locale);
        return dateFormat.format(date);
    }

    public static String getTime(long time){
        Date date=new Date(time);
        Locale locale = new Locale("ru","RU");
        SimpleDateFormat dateFormat =new SimpleDateFormat("HH:mm", locale);
        return dateFormat.format(date);
    }

    public static String FormatStringAsPhoneNumber(String input) {
        String output;
        switch (input.length()) {
            case  0:{
                output=input;
                break;
            }
            case 1:{
                output=input;
                break;
            }
            case 2:{
                output=input;
                break;
            }
            case 3:
                output = String.format("%s", input.substring(0,3));
                break;
            case 4:{
                output = String.format("(%s)%s", input.substring(0,3),input.substring(3));
                break;
            }
            case 5:{
                output = String.format("(%s)%s", input.substring(0,3),input.substring(3));
                break;
            }
            case 6: {
                output = String.format("(%s)%s", input.substring(0, 3), input.substring(3));
                break;
            }
            case 7:{
                output = String.format("(%s)%s-%s", input.substring(0,3),input.substring(3,6),input.substring(6));
                break;
            }
            case 8:{
                output = String.format("(%s)%s-%s", input.substring(0,3),input.substring(3,6),input.substring(6));
                break;
            }
            case  9:{
                output = String.format("(%s)%s-%s-%s", input.substring(0,3),input.substring(3,6),input.substring(6,8),input.substring(8,9));
                break;
            }
            case 10: {
                output = String.format("(%s)%s-%s-%s", input.substring(0, 3), input.substring(3, 6), input.substring(6, 8), input.substring(8,10));
                break;
            }
            default:
                output=null;
                break;

        }
        return output;
    }


}
